/*
 * Author: LY
 * JUnit test for adding movies to a (local) database
 */
package com.movieapp.client.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Test;

import com.movieapp.client.Movie;

public class AddFilmTest {

	@Test
	public void testAddFilm() {
		
			// Create a test-film1 for testing Export function
			ArrayList<String> language = new ArrayList<String>();
			language.add("German");
			language.add("English");
			ArrayList<String> genre = new ArrayList<String>();
			genre.add("Romance");
			ArrayList<String> country = new ArrayList<String>();
			country.add("England");
			country.add("United States of America");
			country.add("Switzerland");
			//Movie film1 = new Movie("film1", "1967", "92.5", language, genre, country);

			// Create a test-film2 for testing Export function
			ArrayList<String> language2 = new ArrayList<String>();
			language2.add("Spanish");
			language2.add("English");
			language2.add("Swedish");
			ArrayList<String> genre2 = new ArrayList<String>();
			genre2.add("Science Fiction");
			genre2.add("Action");
			ArrayList<String> country2 = new ArrayList<String>();
			country2.add("England");
			country2.add("Switzerland");
			//Movie film2 = new Movie("film2", "2100", "10.5567", language2, genre2, country2);
			
			/*assertEquals(2, film1.getLanguage().size());
			assertEquals(1, film1.getGenres().size());
			assertEquals(3, film1.getCountries().size());

			assertEquals(3, film2.getLanguage().size());
			assertEquals(2, film2.getGenres().size());
			assertEquals(2, film2.getCountries().size());
			
			assertTrue(film1.getLanguage().get(0).equals("German"));
			assertTrue(film1.getLanguage().get(1).equals("English"));
			assertTrue(film1.getGenres().get(0).equals("Romance"));
			assertTrue(film1.getCountries().get(0).equals("England"));
			assertTrue(film1.getCountries().get(1).equals("United States of America"));
			assertTrue(film1.getCountries().get(2).equals("Switzerland"));
			
			assertTrue(film2.getLanguage().get(0).equals("Spanish"));
			assertTrue(film2.getLanguage().get(1).equals("English"));
			assertTrue(film2.getLanguage().get(2).equals("Swedish"));
			assertTrue(film2.getGenres().get(0).equals("Science Fiction"));
			assertTrue(film2.getGenres().get(1).equals("Action"));
			assertTrue(film2.getCountries().get(0).equals("England"));
			assertTrue(film2.getCountries().get(1).equals("Switzerland"));
			
			
			tempDatabase testdata = new tempDatabase();
			testdata.addMovie(film1);
			testdata.addMovie(film2);
			
			assertEquals(2, testdata.getDatabase().size());
		
			assertTrue(testdata.getDatabase().get(0).equals(film1));
			assertTrue(testdata.getDatabase().get(1).equals(film2));*/
			
	}

}
