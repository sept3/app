package com.movieapp.client.test;

import static org.junit.Assert.*;

import java.util.ArrayList;


import org.junit.Before;
import org.junit.Test;

import com.movieapp.client.DBServiceMethods;
import com.movieapp.client.Movie;
import com.movieapp.client.MovieCriterias;
import com.movieapp.client.MovieList;
import com.movieapp.server.Database;

public class DBTest {

	@Before
	public void setUp() throws Exception {
		Movie tempMovie = new Movie();
		MovieList mL = new MovieList();
		tempMovie.initMovie("12345", "54321", "Test", "1900", "1000","90", "english", "USA", "Thriller");
		mL.addMovie(tempMovie);
		Database.addMovieList(mL);
	}

	@Test
	public void test() {
		//MovieList mL1 = FileToMovieList.convertFileToMovieList("./movies_80000.tsv");
		//assertTrue(mL1.getMovies().get(0).getName().equals("Ghosts of Mars"));
		MovieList mL = new MovieList();
		for (int i=0; i<10; i++)
		{
			Movie tempMovie = new Movie();
			tempMovie.initMovie("12345", "54321", "Test", "1900", "1000","90", "english", "USA", "Thriller");
			mL.addMovie(tempMovie);
			assertTrue(tempMovie.getWikiMovID().equals("12345"));
			assertTrue(tempMovie.getFreeBID().equals("54321"));
			assertTrue(tempMovie.getName().equals("Test"));
			assertTrue(tempMovie.getYear().equals("1900"));
			assertTrue(tempMovie.getRevenue() == 1000);
			assertTrue(tempMovie.getRuntime() == 90);
			assertTrue(tempMovie.getLanguage().get(0).equals("english"));
			assertTrue(tempMovie.getCountry().get(0).equals("USA"));
			assertTrue(tempMovie.getGenres().get(0).equals("Thriller"));
			assertTrue(mL.getMovies().get(i) != null);
			assertTrue(tempMovie.toString().equals("Wikipedia Movie ID: 12345 FreeBase ID: "
				+ "54321 Name: Test Year: 1900 Runtime: 90 Revenue: 1000 Language: english Country: "
				+ "USA Genres: Thriller"));
		}
		ArrayList<MovieCriterias> tempMC = new ArrayList<MovieCriterias>();
		ArrayList<String> criterias = new ArrayList<String>();
		tempMC.add(MovieCriterias.YEAR);
		criterias.add("1900");
		System.out.println(Integer.toString(Database.addMovieList(mL)));
		/*System.out.println(Integer.toString(Database.mL.size()));*/
		assertTrue(Database.getMovieList(tempMC, criterias).getMovies().get(0).getYear().equals("1900"));
		Database.setCriteria(true, MovieCriterias.Genres, 0, 0);
		assertTrue(Database.getCriteriasGenres().size()>0);
		assertTrue(Database.getFirst().getMovies().get(0).getName().equals("Test"));
		
		DBServiceMethods.getCriteriaName(0);
		assertTrue(DBServiceMethods.resultGetCriteriaName!=null);
		}

}
