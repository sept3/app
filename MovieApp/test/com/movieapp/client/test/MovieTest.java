package com.movieapp.client.test;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import com.movieapp.client.Movie;

public class MovieTest {

	Movie testMovie;
	
	@Before
	public void setUp() throws Exception {
		testMovie = new Movie();
		testMovie.initMovie("sdfsdf", "sadfdsaf", "asdfsadfdsf", "Der Name der Rose", "sdafasdf", "asdfasdf", "asdfsadf", "US", "asdfasdfsadf");
	}

	@Test
	public void testInitMethod() {
		assertTrue(testMovie.getCountry()!=null);
		assertTrue(testMovie.getFreeBID()!=null);
		assertTrue(testMovie.getGenres()!=null);
		assertTrue(testMovie.getLanguage()!=null);
		assertTrue(testMovie.getName()!=null);
		assertTrue(testMovie.getRevenue()==0);
		assertTrue(testMovie.getWikiMovID()!=null);
		assertTrue(testMovie.getYear()!=null);
	}

}
