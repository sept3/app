package com.movieapp.server;

import java.io.BufferedReader;
import java.io.FileReader;
import com.movieapp.client.MovieList;
import com.movieapp.client.Movie;

public class FileToMovieList {

	public static String errmess;
	public static String sysOut;
	
	/*Convert a file to a Movielist
	 *@param Path to the file. 
	 */
	public static MovieList convertFileToMovieList(String pathToFile)
	{
		MovieList mL = new MovieList();
		mL.initMovieList("Test", "Test");
		BufferedReader input = readFile(pathToFile);
		if (input == null) 
		{
			errmess = "INPUT = NULL";
		}
		else 
		{
			String ss;
			try 
			{
				while ((ss=input.readLine())!=null)
				{
					mL.addMovie(strArrayToMovie(disassembleInputLine(ss)));
					//break;
				}
			}
			catch (Exception e) 
			{
				errmess = e.toString();
			}
		}
		return mL;
	}
	
	/* Reads the file
	 * @param path to the file.
	 */
	// Reads the input file and saves it in a BufferedReader
	private static BufferedReader readFile(String pathToFile)
	{
		try 
		{
			BufferedReader in = new BufferedReader(new FileReader(DBServiceImpl.fullPath));//../../war/WEB_INF/movies_80000.tsv"));
			return in;
		} 	
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/* String array to a movie.
	 * @param String array that should be casted.
	 */
	private static Movie strArrayToMovie(String[] movie)
	{
		for (int i=6; i<9; i++)
		{
			movie[i] = cleanString(movie[i]);
		}
		Movie tM = new Movie();
		tM.initMovie(movie[0], movie[1], movie[2], movie[3], movie[4], movie[5],  movie[6], movie[7], movie[8]);
		//midStep = tM.toString();
		//tM.initMovie(movie[2], movie[3], movie[6], movie[7], movie[8]);
		//System.out.println(tM.toString());
		return tM;
	}
	

	/* Disassembles the Read File (TSV) and stores it in a String array
	 * @param Strin to disassemble.
	 */
	private static String[] disassembleInputLine(String line) 
	{
		String cutString[] = new String[9];
		
		int counter = 0;
		int msCounter = 0;
		int i = 0;
		for (char c : line.toCharArray()) 
		{
			if (c == '\t') 
			{
				cutString[i] = line.substring(msCounter, counter);
				i++;
				msCounter = counter + 1;
			}
			counter++;
		}
		//String ending doesnt get catched by the if clause
		cutString[i] = line.substring(msCounter);
		return cutString;
	}
	
	/* Remove all unnecessary chars from a given string.
	 * @param string to clean.
	 */
	private static String cleanString(String input)
	{
		String cleanedInput = "";
		int startStr = 0;
		int counter = 0;
		boolean firstStr = true;
		
		for (int i=0; i<input.length(); i++) 
		{
			if (input.charAt(i)== '"')
			{
				counter++;
				if (counter == 4)
				{
					if (!firstStr)
					{
						cleanedInput += "," + input.substring(startStr, i);
					}
					else
					{
						cleanedInput = input.substring(startStr, i);
						firstStr = false;
					}
					counter = 0;
				}
				else if (counter == 3)
				{
					startStr = i + 1;
				}
				else 
				{}
			}
		}
		return cleanedInput;
	}
	

}
