package com.movieapp.server;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.servlet.ServletContext;

import com.movieapp.client.DBService;
import com.movieapp.client.MovieCriterias;
import com.movieapp.client.MovieList;
import com.movieapp.client.Movie;
import com.movieapp.shared.FieldVerifier;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class DBServiceImpl extends RemoteServiceServlet implements DBService
{
	private boolean firstCall = true;
	
	public static String fullPath;

	public Boolean getCall() //throws IllegalArgumentException 
	{
		return firstCall;
	}
	
	public MovieList getMovieList(ArrayList<MovieCriterias> criteriaType, ArrayList<String> criteria) //throws IllegalArgumentException 
	{
		
		return Database.getMovieList(criteriaType, criteria);
	}
	
	public MovieList getNextList(int indexOfNext)
	{
		return Database.getNextList(indexOfNext);
	}
	
	public String readIntoDB() //throws IllegalArgumentException 
	{
		firstCall = false;
		String movieFile = "movies_80000.tsv";
		String result = "";
		ServletContext context = getServletContext();
		fullPath = context.getRealPath(movieFile);
		MovieList mL = FileToMovieList.convertFileToMovieList(fullPath);
		Database.addMovieList(mL);
		movieFile = "movies_1471.tsv";
		fullPath = context.getRealPath(movieFile);
		mL = FileToMovieList.convertFileToMovieList(fullPath);
		Database.addMovieList(mL);
		result = Database.getML().get(0).getMovies().get(0).toString();
		return result;
	}
	
	public String sysOut() //throws IllegalArgumentException 
	{
		String result = "Start Value";
		result = Database.sysOut;
		return result;
	}
	
	
	public String getResult()
	{
		//String result = Database.mL.get(0).getMovies().get(0).toString();
		String result = "";
		//Database.setCriteria();
		//result = Database.getCriteriasLanguage().get(0);
		for (int i=0;i<20;i++)
		{	
			result += ", " + Database.getCriteriasGenres().get(i);
			//result += ", " + Database.getML().get(0).getMovies().get(0).getGenres().get(i);
		}
		//result = Database.mL.get(0).getMovies().get(0).getLanguage();
		/*for(int i=0;i<5;i++)
		{
			if(i==0)
			{
				result = Database.getML().get(0).getMovies().get(i).toString();
			}
			else
			{
				result += "\n " + result;
			}
		}*/
		return result;
	}
	
	public MovieList testObject()
	{
		return Database.getFirst();
		//return result;
	}
	
	public int[] loadCriteria(boolean isFirst, MovieCriterias criteriaType, int beginI, int beginJ)
	{
		return Database.setCriteria(isFirst, criteriaType, beginI, beginJ);
		//return "ok";
	}
	
	/*public ArrayList<String> getCriteria(MovieCriterias criteria)
	{
		switch (criteria)
		{
			case WIKIID:
				return Database.getCriteriasWID();
			case FREEID:
				return Database.getCriteriasFID();
			case NAME:
				return Database.getCriteriasName();
			case YEAR:
				return Database.getCriteriasYear();
			case REVENUE:
				return Database.getCriteriasRevenue();
			case RUNTIME:
				return Database.getCriteriasRuntime();
			case LANGUAGE:
				return Database.getCriteriasLanguage();
			case COUNTRY:
				return Database.getCriteriasCountry();
			case GENRES:
				return Database.getCriteriasGenres();
		}
		return null;
	}*/
	
	public ArrayList<String> getCriteriaWID(int nextIndex)
	{
		ArrayList<String> tempWID = new ArrayList<String>();
		for (int i=(nextIndex); i<(10000+nextIndex+1) && i<Database.getCriteriasWID().size(); i++)
		{
			if (i==(nextIndex+10000) && i != 0)
			{
				tempWID.add(Integer.toString(nextIndex+10000));
			}
			else
			{
				tempWID.add(Database.getCriteriasWID().get(i));
			}
		}
		return tempWID;
	}
	
	public ArrayList<String> getCriteriaFID(int nextIndex)
	{
		ArrayList<String> tempFID = new ArrayList<String>();
		for (int i=(nextIndex); i<(10000+nextIndex+1) && i<Database.getCriteriasFID().size(); i++)
		{
			if (i==(nextIndex+10000)  && i != 0)
			{
				tempFID.add(Integer.toString(nextIndex+10000));
			}
			else
			{
				tempFID.add(Database.getCriteriasFID().get(i));
			}
		}
		return tempFID;
	}
	
	public ArrayList<String> getCriteriaName(int nextIndex)
	{
		ArrayList<String> tempName = new ArrayList<String>();
		for (int i=nextIndex; i<(10000+nextIndex+1) && i<Database.getCriteriasName().size(); i++)
		{
			if (i==(nextIndex+10000) && i != 0)
			{
				tempName.add(Integer.toString(nextIndex+10000));
			}
			else
			{
				tempName.add(Database.getCriteriasName().get(i));
			}
		}
		return tempName;
	}
	
	public ArrayList<String> getCriteriaYear()
	{
		return Database.getCriteriasYear();
	}
	
	public ArrayList<String> getCriteriaRevenue()
	{
		return Database.getCriteriasRevenue();
	}
	
	public ArrayList<String> getCriteriaRuntime()
	{
		return Database.getCriteriasRuntime();
	}
	
	public ArrayList<String> getCriteriaLanguage()
	{
		return Database.getCriteriasLanguage();
	}
	
	public ArrayList<String> getCriteriaCountry()
	{
		return Database.getCriteriasCountry();
	}
	
	public ArrayList<String> getCriteriaGenres()
	{
		return Database.getCriteriasGenres();
	}
	
	
	
	public LinkedList<Movie> testRPC()
	{
		/*String movieFile = "movies_80000.tsv";
		ServletContext context = getServletContext();
		fullPath = context.getRealPath(movieFile);
		MovieList mL = FileToMovieList.convertFileToMovieList(fullPath);*/
		LinkedList<Movie> llm = new LinkedList<Movie>();
		llm.add(Database.getML().get(0).getMovies().get(0));
		return llm;
	}
		/*// Verify that the input is valid. 
		if (!FieldVerifier.isValidName(input)) {
			// If the input is not valid, throw an IllegalArgumentException back to
			// the client.
			throw new IllegalArgumentException(
					"Name must be at least 4 characters long");
		}

		String serverInfo = getServletContext().getServerInfo();
		String userAgent = getThreadLocalRequest().getHeader("User-Agent");

		// Escape data from the client to avoid cross-site script vulnerabilities.
		input = escapeHtml(input);
		userAgent = escapeHtml(userAgent);

		return "Hello, " + input + "!<br><br>I am running " + serverInfo
				+ ".<br><br>It looks like you are using:<br>" + userAgent;
	}*/

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");
	}
}
