package com.movieapp.server;

//@LS
import java.util.ArrayList;
import java.util.LinkedList;

import com.movieapp.client.Movie;
import com.movieapp.client.MovieCriterias;
import com.movieapp.client.MovieList;


public class Database 
{
	public static String sysOut = "start";
	private static ArrayList<MovieList> mL = new ArrayList<MovieList>();
	private static ArrayList<String> criteriasWID = new ArrayList<String>();
	private static ArrayList<String> criteriasFID = new ArrayList<String>();
	private static ArrayList<String> criteriasName = new ArrayList<String>();
	private static ArrayList<String> criteriasYear = new ArrayList<String>();
	private static ArrayList<String> criteriasRevenue = new ArrayList<String>();
	private static ArrayList<String> criteriasRuntime = new ArrayList<String>();
	private static ArrayList<String> criteriasGenres = new ArrayList<String>();
	private static ArrayList<String> criteriasLanguage = new ArrayList<String>();
	private static ArrayList<String> criteriasCountry = new ArrayList<String>();
	
	private static ArrayList<MovieList> rpcLists = new ArrayList<MovieList>();
	
	/*Get first movie of DB*/
	public static MovieList getFirst()
	{
		MovieList temp = new MovieList();
		temp.addMovie(mL.get(0).getMovies().get(0));
		return temp;
	}
	
	/*add MovieList to DB
	 *@param Movie list to add. 
	 */
	public static void addMovieList(MovieList newMovieList)
	{
			mL.add(newMovieList);
	}
	
	/*Get movielist with the filter criteria
	public static MovieList getMovieList(String criteria)
	{
		String src = "";
		for (int i=0; i<mL.size(); i++) 
		{
			src += src + mL.get(i).getName() + ";"; 
		}
		MovieList filteredML = new MovieList();
		
		for (int j=0; j<mL.size(); j++)	
		{
			for (int h=0; h<mL.get(j).getMovies().size(); h++)
			{
				if (mL.get(j).getMovies().get(h).getName() == criteria)
				{	
					filteredML.addMovie(mL.get(j).getMovies().get(h));
				}
			}
		}
		return filteredML;
	}*/
	
	/*Set all possible criteria 
	 *@param For the handling of a long rpc: If its a repeated request or the first run.
	 *@param For the handling of a long rpc: The criteria that should be loaded in the next request.
	 *@param For the handling of a long rpc: starting point of next i in next request.
	 *@param For the handling of a long rpc: Starting point of next j in next request.
	 */
	public static int[] setCriteria(boolean firstRun, MovieCriterias criteriaType, int startI, int startJ)
	{
		int indexI;
		int indexJ;
		//first is if still ongoin, second criteria Type, two last are indexes for next run
		int returnVals[] = new int[4];
		if (firstRun)
		{
			criteriasWID.clear();
			criteriasFID.clear();
			criteriasName.clear();
			criteriasYear.clear();
			criteriasRevenue.clear();
			criteriasRuntime.clear();
			criteriasLanguage.clear();
			criteriasCountry.clear();
			criteriasGenres.clear();
			indexI = 0;
			indexJ = 0;
		}
		else 
		{
			indexI = startI;
			indexJ = startJ;
		}
		
		
		
		
		for (int i=indexI; i<mL.size(); i++)
		{
			switch (criteriaType)
			{
			case WIKIID:
			{
				// ***************************** WID *******************************
			
				for (int j=indexJ; j<10000+indexJ && j<mL.get(i).getMovies().size(); j++)
					{
					int temp = 0;
					try
					{
						criteriasWID.add(mL.get(i).getMovies().get(j).getWikiMovID());	
						if (j==10000+indexJ-1 && j!=mL.get(i).getMovies().size()-1)
						{
							returnVals[0] = 1;
							returnVals[1] = 0;
							returnVals[2] = i;
							returnVals[3] = j+1;
							return returnVals;
						}
						else if(j==mL.get(i).getMovies().size()-1)
						{
							returnVals[0] = 1;
							returnVals[1] = 1;
							returnVals[2] = i;
							returnVals[3] = 0;
							return returnVals;
						}
					}
					catch(Exception e)
					{
						sysOut = Integer.toString(temp);
					}
				}
			}
			break;
			case FREEID:
			{
				// ***************************** FID *******************************
		
				for (int j=indexJ; j<10000+indexJ && j<mL.get(i).getMovies().size(); j++)
				{
					criteriasFID.add(mL.get(i).getMovies().get(j).getFreeBID());
					if (j==10000+indexJ-1 && j!=mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 1;
						returnVals[2] = i;
						returnVals[3] = j+1;
						return returnVals;
					}
					else if(j==mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 2;
						returnVals[2] = i;
						returnVals[3] = 0;
						return returnVals;
					}
				}
			}
			break;
			case NAME:
			{
				// ***************************** NAME *******************************
				//Not stable if movie names could be twice, but else it needs way too long
				for (int j=indexJ; j<10000+indexJ && j<mL.get(i).getMovies().size(); j++)
				{
					criteriasName.add(mL.get(i).getMovies().get(j).getName());
					if (j==10000+indexJ-1 && j!=mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 2;
						returnVals[2] = i;
						returnVals[3] = j+1;
						return returnVals;
					}
					else if(j==mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 3;
						returnVals[2] = i;
						returnVals[3] = 0;
						return returnVals;
					}
				}
			}
			break;
			case YEAR:
			{
			
				// ***************************** Year *******************************
				for (int j=indexJ; j<10000+indexJ && j<mL.get(i).getMovies().size(); j++)
				{
					if (!mL.get(i).getMovies().get(j).getYear().equals("")
							&& isNotInList(criteriasYear, mL.get(i).getMovies().get(j).getYear()))
					{
						criteriasYear.add(mL.get(i).getMovies().get(j).getYear());
					}
					if (j==10000+indexJ-1 && j!=mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 3;
						returnVals[2] = i;
						returnVals[3] = j+1;
						return returnVals;
					}
					else if(j==mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 4;
						returnVals[2] = i;
						returnVals[3] = 0;
						return returnVals;
					}
				}
			}
			break;
			case REVENUE:
			{
				// ***************************** Revenue *******************************
				for (int j=indexJ; j<10000+indexJ && j<mL.get(i).getMovies().size(); j++)
				{
					if (mL.get(i).getMovies().get(j).getRevenue() != 0 
						&& isNotInList(criteriasRevenue, String.valueOf(mL.get(i).getMovies().get(j).getRevenue())))
					{
						criteriasRevenue.add(String.valueOf(mL.get(i).getMovies().get(j).getRevenue()));
					}
					if (j==10000+indexJ-1 && j!=mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 4;
						returnVals[2] = i;
						returnVals[3] = j+1;
						return returnVals;
					}
					else if(j==mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 5;
						returnVals[2] = i;
						returnVals[3] = 0;
						return returnVals;
					}
								
				}
			}
			break;
			case RUNTIME:
			{
				// ***************************** Runtime *******************************
				for (int j=indexJ; j<10000+indexJ && j<mL.get(i).getMovies().size(); j++)
				{
					//has to cancel if equals 0
					if (mL.get(i).getMovies().get(j).getRuntime() != 0 
						&& isNotInList(criteriasRuntime, String.valueOf(mL.get(i).getMovies().get(j).getRuntime())))
					{
						criteriasRuntime.add(String.valueOf(mL.get(i).getMovies().get(j).getRuntime()));
					}
					if (j==10000+indexJ-1 && j!=mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 5;
						returnVals[2] = i;
						returnVals[3] = j+1;
						return returnVals;
					}
					else if(j==mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 6;
						returnVals[2] = i;
						returnVals[3] = 0;
						return returnVals;
					}
				}
			}
			break;
			
			case LANGUAGE:
			{
				// ***************************** Language *******************************
				for (int j=indexJ; j<10000+indexJ && j<mL.get(i).getMovies().size(); j++)
				{
					for (int h=0;h<mL.get(i).getMovies().get(j).getLanguage().size();h++)
					{
						if (!mL.get(i).getMovies().get(j).getLanguage().get(h).equals("")
							&& isNotInList(criteriasLanguage, mL.get(i).getMovies().get(j).getLanguage().get(h)))
						{
							criteriasLanguage.add(mL.get(i).getMovies().get(j).getLanguage().get(h));
						}
					}
					if (j==10000+indexJ-1 && j!=mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 6;
						returnVals[2] = i;
						returnVals[3] = j+1;
						return returnVals;
					}
					else if(j==mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 7;
						returnVals[2] = i;
						returnVals[3] = 0;
						return returnVals;
					}
				}
			}
			break;
			case COUNTRY:
			{
				// ***************************** Country *******************************
				for (int j=indexJ; j<10000+indexJ && j<mL.get(i).getMovies().size(); j++)
				{
					for (int h=0;h<mL.get(i).getMovies().get(j).getCountry().size();h++)
					{
						if (!mL.get(i).getMovies().get(j).getCountry().get(h).equals("")
							&& isNotInList(criteriasCountry, mL.get(i).getMovies().get(j).getCountry().get(h)))
						{
							criteriasCountry.add(mL.get(i).getMovies().get(j).getCountry().get(h));
						}
					}
					if (j==10000+indexJ-1 && j!=mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 7;
						returnVals[2] = i;
						returnVals[3] = j+1;
						return returnVals;
					}
					else if(j==mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 8;
						returnVals[2] = i;
						returnVals[3] = 0;
						return returnVals;
					}
				}
			}
			break;
			case GENRES:
			{
				// ***************************** Genres *******************************
				for (int j=indexJ; j<10000+indexJ && j<mL.get(i).getMovies().size(); j++)
				{
					for (int h=0;h<mL.get(i).getMovies().get(j).getGenres().size();h++)
					{	
						if (!mL.get(i).getMovies().get(j).getGenres().get(h).equals("")
							&& isNotInList(criteriasGenres, mL.get(i).getMovies().get(j).getGenres().get(h)))
						{
							criteriasGenres.add(mL.get(i).getMovies().get(j).getGenres().get(h));
						}
					}
					if (j==10000+indexJ-1 && j!=mL.get(i).getMovies().size()-1)
					{
						returnVals[0] = 1;
						returnVals[1] = 8;
						returnVals[2] = i;
						returnVals[3] = j+1;
						return returnVals;
					}
					else if(i!=mL.size()-1 && j==mL.get(i).getMovies().size()-1)
					{
						//start at wid again
						returnVals[0] = 1;
						returnVals[1] = 0;
						returnVals[2] = i+1;
						returnVals[3] = 0;
						return returnVals;
					}
				}
			}
			}
		}
		returnVals[0] = 0;
		returnVals[1] = 0;
		returnVals[2] = 0;
		returnVals[3] = 0;
		return returnVals;
	}
	
	/*Checks if the string is already in list
	 * @param Criteria list to check.
	 * @param String to check
	 */
	private static boolean isNotInList(ArrayList<String> nameOfList, String strToCheck)
	{
		int temp = 0;
		try{
		for (int i=0;i<nameOfList.size();i++)
		{
			if (strToCheck.replaceAll("\\s+","").equalsIgnoreCase(nameOfList.get(i).replaceAll("\\s+","")))
			{
				return false;
			}
			temp = i;
		}
		return true;
		}
		catch (Exception e)
		{
			sysOut = Integer.toString(temp) + "Size:" + nameOfList.size();
			return false;
		}
	}
	
	/*Searches for Movies for the given pattern criteria.
	 * @param: All criterias that should be searched.
	 * @param: Pattern for the criterias.
	 */
	public static MovieList getMovieList(ArrayList<MovieCriterias> criteriaTypeInput, ArrayList<String> criteriaInput)
	{
		for (int i=0; i<criteriaInput.size(); i++)
		{
			if (!(criteriaInput.get(i).trim().equals("")))
			{
				break;
			}
			else
			{
				if (i == criteriaInput.size()-1)
				{
					return null;
				}
			}
		}
		if (criteriaTypeInput.size() == 0 || criteriaInput.size()==0)
		{
			return null;
		}
		
		rpcLists.clear();
		sysOut = "1";
		MovieList midML = new MovieList();
		int listCounter = 0;
		
		MovieCriterias criteriaType = criteriaTypeInput.get(0);
		String criteria = criteriaInput.get(0);
		
		for (int i=0; i<mL.size(); i++)
		{
			sysOut = "2";
			for (int j=0;j<mL.get(i).getMovies().size(); j++)
			{
				if (!(midML.getMovies().size() <= 1000))
				{
					sysOut = "3";
					midML.setIsLast(false);
					midML.setIndexOfNext(listCounter+1);
					rpcLists.add(midML);
					midML = null;
					midML = new MovieList();
						listCounter++;
				}
				sysOut = "4";
				switch (criteriaType)
				{
					case WIKIID:
						sysOut = "W";
						if (mL.get(i).getMovies().get(j).getWikiMovID().trim().equals(criteria))
						{
							midML.addMovie(mL.get(i).getMovies().get(j));
						}
						break;
					case FREEID:
						sysOut += "F";
						if (mL.get(i).getMovies().get(j).getFreeBID().trim().equals(criteria))
						{
							midML.addMovie(mL.get(i).getMovies().get(j));
						}
						break;
					case NAME:
						sysOut += "N";
						if (mL.get(i).getMovies().get(j).getName().equals(criteria))
						{
							midML.addMovie(mL.get(i).getMovies().get(j));
						}
						break;
					case YEAR:
						sysOut += Integer.toString(j);
						if (j ==2)
						{sysOut = mL.get(i).getMovies().get(j).getYear().trim() + " : " + criteria.trim();}
						if (mL.get(i).getMovies().get(j).getYear().equals(criteria))
						{
							if (criteriaTypeInput.size()>1)
							{
								if (checkAllCriteria(criteriaTypeInput, criteriaInput, mL.get(i).getMovies().get(j)))
								{
									midML.addMovie(mL.get(i).getMovies().get(j));
								}
							}
							else
							{
								midML.addMovie(mL.get(i).getMovies().get(j));
							}
						}
						break;
					case REVENUE:
						if (Integer.toString(mL.get(i).getMovies().get(j).getRevenue()).equals(criteria))
						{
							if (criteriaTypeInput.size()>1)
							{
								if (checkAllCriteria(criteriaTypeInput, criteriaInput, mL.get(i).getMovies().get(j)))
								{
									midML.addMovie(mL.get(i).getMovies().get(j));
								}
							}
							else
							{
								midML.addMovie(mL.get(i).getMovies().get(j));
							}
						}
					case RUNTIME:
						if (Integer.toString(mL.get(i).getMovies().get(j).getRuntime()).equals(criteria))
						{
							if (criteriaTypeInput.size()>1)
							{
								if (checkAllCriteria(criteriaTypeInput, criteriaInput, mL.get(i).getMovies().get(j)))
								{
									midML.addMovie(mL.get(i).getMovies().get(j));
								}
							}
							else
							{
								midML.addMovie(mL.get(i).getMovies().get(j));
							}
						}
						break;
					case LANGUAGE:
						for (int h=0; h<mL.get(i).getMovies().get(j).getLanguage().size();h++)
						{
							if (mL.get(i).getMovies().get(j).getLanguage().get(h).equalsIgnoreCase(criteria))
							{
								if (criteriaTypeInput.size()>1)
								{
									if (checkAllCriteria(criteriaTypeInput, criteriaInput, mL.get(i).getMovies().get(j)))
									{
										midML.addMovie(mL.get(i).getMovies().get(j));
									}
								}
								else
								{
									midML.addMovie(mL.get(i).getMovies().get(j));
								}
								break;
							}
						}
						break;
					case COUNTRY:
						for (int h=0; h<mL.get(i).getMovies().get(j).getCountry().size();h++)
						{
							if (mL.get(i).getMovies().get(j).getCountry().get(h).equalsIgnoreCase(criteria))
							{
								if (criteriaTypeInput.size()>1)
								{
									if (checkAllCriteria(criteriaTypeInput, criteriaInput, mL.get(i).getMovies().get(j)))
									{
										midML.addMovie(mL.get(i).getMovies().get(j));
									}
								}
								else
								{
									midML.addMovie(mL.get(i).getMovies().get(j));
								}
								break;
							}
						}
						break;
					case GENRES:
						for (int h=0; h<mL.get(i).getMovies().get(j).getGenres().size();h++)
						{
							if (mL.get(i).getMovies().get(j).getGenres().get(h).equalsIgnoreCase(criteria))
							{
								if (criteriaTypeInput.size()>1)
								{
									if (checkAllCriteria(criteriaTypeInput, criteriaInput, mL.get(i).getMovies().get(j)))
									{
										midML.addMovie(mL.get(i).getMovies().get(j));
									}
								}
								else
								{
									midML.addMovie(mL.get(i).getMovies().get(j));
								}
								break;
							}
						}
						break;
				}
			}
		}	
		midML.setIsLast(true);
		if (rpcLists.size()>1)
		{
			rpcLists.get(rpcLists.size()-1).setIsLast(true);
		}
		//Clean if only one MovieList necessary
		if (rpcLists.size() == 0)
		{
			//sysOut =midML.getMovies().get(0).toString();
			return midML;
		}
		else 
		{
			rpcLists.add(midML);
			return rpcLists.get(0); //rpcLists.get(0);
		}
	}
	
	/* For the handling of the rpc for too big lists. 
	 * @param Index of the index, of the next list that should be sent to client.
	 */
	public static MovieList getNextList(int indexOfNext)
	{
		MovieList tempML = new MovieList();
		tempML = rpcLists.get(indexOfNext);
		
		if (tempML.getIsLast())
		{
			rpcLists.clear();
		}
		return tempML;
	}
	
	/*
	 * Check if the all input criteria matches the movie.
	 * @param CriteriaTypes that should be checked.
	 * @param Criteria that should match.
	 * @param The Movie that has to get checked.
	 * 
	 */
	private static boolean checkAllCriteria(ArrayList<MovieCriterias> criteriaTypes, ArrayList<String> criterias, Movie toCheck)
	{
		// 0 already checking in getMovieList method
		for (int i=1; i<criteriaTypes.size(); i++)
		{
			switch (criteriaTypes.get(i))
			{
			//WID, FID, Name and Year will always already be running so there is no need to check them
			case REVENUE:
				if (!(criterias.get(i).trim().equals(Integer.toString(toCheck.getRevenue()).trim())))
				{
					return false;
				}
				break;
			case RUNTIME:
				if (!(criterias.get(i).trim().equals(Integer.toString(toCheck.getRuntime()).trim())))
				{
					return false;
				}
				break;
			case LANGUAGE:
				for (int j=0; j<toCheck.getLanguage().size(); j++)
				{
					if (!(criterias.get(i).equals(toCheck.getLanguage().get(j))))
					{
						if (j==toCheck.getLanguage().size()-1)
						{
							return false;
						}
					}
					else
					{
						break;
					}
				}
				break;
			case COUNTRY:
				for (int j=0; j<toCheck.getCountry().size(); j++)
				{
					if (!(criterias.get(i).equals(toCheck.getCountry().get(j))))
					{
						if (j==toCheck.getCountry().size()-1)
						{
							return false;
						}
					}
					else
					{
						break;
					}
				}
				break;
			case GENRES:
				for (int j=0; j<toCheck.getGenres().size(); j++)
				{
					if (!(criterias.get(i).equals(toCheck.getGenres().get(j))))
					{
						if (j==toCheck.getGenres().size()-1)
						{
							return false;
						}
					}
					else
					{
						break;
					}
				}
				break;
			}
		}
		return true;
	}
	
	public static ArrayList<String> getCriteriasLanguage() {
		return criteriasLanguage;
	}

	public static ArrayList<String> getCriteriasCountry() {
		return criteriasCountry;
	}

	public static ArrayList<String> getCriteriasGenres()
	{
		return criteriasGenres;
	}
	
	public static ArrayList<MovieList> getML() {
		return mL;
	}
	
	public static ArrayList<MovieList> getWaitingLists() {
		return rpcLists;
	}

	public static ArrayList<String> getCriteriasWID() {
		return criteriasWID;
	}

	public static ArrayList<String> getCriteriasFID() {
		return criteriasFID;
	}

	public static ArrayList<String> getCriteriasName() {
		return criteriasName;
	}

	public static ArrayList<String> getCriteriasYear() {
		return criteriasYear;
	}

	public static ArrayList<String> getCriteriasRevenue() {
		return criteriasRevenue;
	}

	public static ArrayList<String> getCriteriasRuntime() {
		return criteriasRuntime;
	}

}
