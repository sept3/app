package com.movieapp.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.Window;

public class Login extends Composite{

	private VerticalPanel verticalPanel = new VerticalPanel();
	private Label adminLabel=new Label("Admin please sign in");
	private FlexTable flexTable = new FlexTable();
	private Label username = new Label("Username");
	private TextBox nameentry = new TextBox();
	private TextBox passentry = new TextBox();
	private Label password = new Label("Password");
	private Button loginButton = new Button("Login");
	
	
	
	public Login(){
		verticalPanel.add(adminLabel);
		flexTable.add(username);
		flexTable.add(nameentry);
		flexTable.add(password);
		flexTable.add(passentry);
		loginButton.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				if (nameentry.getText().length() == 0
						|| passentry.getText().length() == 0) {
						Window.alert("Username or password is empty."); 
					}
			}
		});
		flexTable.add(loginButton);
		verticalPanel.add(flexTable);
	}
}
