package com.movieapp.client;

import com.google.gwt.user.client.Timer;

public class Startup {
	/*Check if startup DB needed*/
	public static void startupDB()
	{
		DBServiceMethods.getCall();
	}
	
	/*Start the DB and load Filter criteria, programmed in chain.*/
	public static void startOK()
	{
		Loading.getloadingScreen().show();
		DBServiceMethods.readyDB();
	}
}
