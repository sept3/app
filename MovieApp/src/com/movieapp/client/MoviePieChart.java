package com.movieapp.client;

import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.PieChart;
import com.googlecode.gwt.charts.client.corechart.PieChartOptions;

public class MoviePieChart extends PieChart {

	private PieChartOptions options;
	private DataTable dataTable;

	
	/*
	 * Default Constructor
	 */
	public MoviePieChart() {
		options = PieChartOptions.create();
	}

	/*
	 * Sets a test dataBase for the piechart to show some data
	 * @pre -
	 * @post dataTable != null
	 */
	public void setTestDataTable() {
		dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.STRING, "SliceLabel");
		dataTable.addColumn(ColumnType.NUMBER, "SliceValues");
		dataTable.addRows(3);
		dataTable.setValue(0, 0, "German");
		dataTable.setValue(0, 1, 10);
		dataTable.setValue(1, 0, "English");
		dataTable.setValue(1, 1, 20);
		dataTable.setValue(2, 0, "French");
		dataTable.setValue(2, 1, 30);
	}

	/*
	 * Draws the piechart
	 * @pre options!=null && dataTable!=null
	 * @post Piechart object gets drawn within the container of the user interface
	 */
	public void draw() {
		super.draw(dataTable, options);
	}

}
