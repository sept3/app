package com.movieapp.client;

import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.Histogram;
import com.googlecode.gwt.charts.client.corechart.HistogramOptions;

public class MovieHistogram extends Histogram{

	private DataTable dataTable;
	private HistogramOptions options;
	
	/*
	 * Default Constructor
	 */
	public MovieHistogram(){
		options = HistogramOptions.create();
	}
	
	/*
	 * Sets a test dataBase for the histogram to show some data
	 * @pre -
	 * @post dataTable != null
	 */
	public void setTestDataTable() {
		dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.STRING, "X-Axis");
		dataTable.addColumn(ColumnType.NUMBER, "Y-Axis");
		dataTable.addRows(3);
		dataTable.setValue(0, 0, "47.30,8.45");
		dataTable.setValue(0, 1, 2424);
		dataTable.setValue(1, 0, "Rome");
		dataTable.setValue(1, 1, 2323);
		dataTable.setValue(2, 0, "Washington");
		dataTable.setValue(2, 1, 2345);
	}
	
	/*
	 * Draws the histogram
	 * @pre options!=null && dataTable!=null
	 * @post Histogram object gets drawn within the container of the user interface
	 */
	public void draw(){
		super.draw(dataTable, options);
	}
}
