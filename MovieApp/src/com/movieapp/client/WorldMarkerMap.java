package com.movieapp.client;

import java.util.ArrayList;

import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.geochart.GeoChart;
import com.googlecode.gwt.charts.client.geochart.GeoChartColorAxis;
import com.googlecode.gwt.charts.client.geochart.GeoChartOptions;
import com.googlecode.gwt.charts.client.options.BackgroundColor;
import com.googlecode.gwt.charts.client.options.DisplayMode;
import com.googlecode.gwt.charts.client.options.Resolution;

/*
 * Wrapper class for the GeoChart-Class in Markers-Mode. Shows a world map with
 * different markers. So far only with test data
 *
 *@author Lenz
 *@version 1.0
 */
public class WorldMarkerMap extends GeoChart {

	private static GeoChartOptions options;
	private static GeoChartColorAxis colorAxis;
	private static DataTable dataTable;

	/*
	 * Default Constructor
	 */
	public WorldMarkerMap() {
		options = GeoChartOptions.create();
		options.setDisplayMode(DisplayMode.MARKERS);
		options.setRegion("world");
	}

	/*
	 * 
	 * Sets the height of the Map
	 * 
	 * @pre this.options != null
	 * 
	 * @post Map has now fixed height
	 * 
	 * @param height
	 */
	public static void setHeight(int height) {
		options.setHeight(height);
	}

	/*
	 * Sets the width of the map
	 * 
	 * @pre this.options != null
	 * 
	 * @post Map has now fixed width
	 * 
	 * @param width
	 */
	public void setWidth(int width) {
		options.setWidth(width);
	}

	/*
	 * Sets the BackgroundColor, the strokeColor and the strokeWidth of the Map
	 * 
	 * @pre this.options != null
	 * 
	 * @post Map has now a background color
	 * 
	 * @param fillColor
	 * 
	 * @param strokeColor
	 * 
	 * @param strokeWidth
	 */
	public static void setBackgroundColor(String fillColor, String strokeColor, int strokeWidth) {
		BackgroundColor color = BackgroundColor.create(fillColor, strokeColor, strokeWidth);
		options.setBackgroundColor(color);
	}

	/*
	 * Sets the markers opacity, 0.0 is fully transparent and 1.0 is fully
	 * visible
	 * 
	 * @pre this.options != null
	 * 
	 * @post Markers on Map are now transparent according to the given double
	 * 
	 * @param opacity
	 */
	public static void setMarkerOpacity(double opacity) {
		options.setMarkerOpacity(opacity);
	}

	/*
	 * Method inherits sets the view of the map to either "world" or country.
	 * Codes for the specific countries and further documentation can be found
	 * here: {@link
	 * com.googlecode.gwt.charts.client.geochart.GeoChartOptions#setRegion
	 * Documentation setRegion}
	 * 
	 * @pre this.options != null
	 * 
	 * @post The map shows the view according to the parameters code, as long as
	 * the code exists.
	 * 
	 * @param threeDigitCode
	 */
	public static void setRegion(String threeDigitCode) {
		options.setRegion(threeDigitCode);
		options.setResolution(Resolution.COUNTRIES);
	}

	/*
	 * Method sets the highest and the lowest colors for the ColorAxis of the
	 * Map. Further Documentation can be found here {@link
	 * com.googlecode.gwt.charts.client.geochart.GeoChartColorAxis#setColors
	 * SetColors()}
	 * 
	 * @pre this.options != null
	 * 
	 * @post The map includes color axis with the given colors as highest and
	 * lowest.
	 * 
	 * @param color1
	 * 
	 * @param color2
	 */
	public static void setColorAxis(String color1, String color2) {
		colorAxis = GeoChartColorAxis.create();
		colorAxis.setColors(color1, color2);
		options.setColorAxis(colorAxis);
	}

	/*
	 * This mehtods sets testData
	 * 
	 * @pre dataTable == null
	 * 
	 * @post The DataTable object is filled with 3 columns of the type String,
	 * Number, Number
	 */
	public static void setDataTable() {
		dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.STRING, "Country");
		dataTable.addColumn(ColumnType.NUMBER, "Number of movies");
	}

	public static void setDataTable(MovieList movieList) {
		dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.STRING, "Location");
		dataTable.addColumn(ColumnType.STRING, "Description");
		StringBuilder sb = new StringBuilder();
		ArrayList<Movie> movieArray = movieList.getMovies();
		dataTable.addRows(movieArray.size());
		for (int i = 0; i < movieArray.size(); i++) {
			sb.append(movieArray.get(i).getName());
			sb.append("\n");
			for (int j = 1; j < movieArray.size(); j++) {
				if (movieArray.get(i).getCountry().get(0).equalsIgnoreCase(movieArray.get(j).getCountry().get(0))) {
					if (!movieArray.get(i).getName().equalsIgnoreCase(movieArray.get(j).getName())) {
						sb.append(movieArray.get(j).getName());
						sb.append("\n");
					}
					movieArray.remove(j);
				}
			}

			dataTable.setValue(i, 0, movieArray.get(i).getCountry().get(0));
			dataTable.setValue(i, 1, sb.toString());
			sb.delete(0, sb.length());
		}

	}

	/*
	 * public static void setYearDataTable(String year) { ArrayList<Movie>
	 * listOfMovies = localMovieList.getMovies(); ArrayList<Movie> shortList =
	 * new ArrayList<Movie>();
	 * 
	 * for (int i = 0; i < listOfMovies.size(); i++) { if
	 * (listOfMovies.get(i).getYear().equalsIgnoreCase(year)) {
	 * shortList.add(listOfMovies.get(i)); } } setDataTable(shortList);
	 * 
	 * 
	 * }
	 */

	/*
	 * Calls the Method draw of the superclass {@link
	 * com.googlecode.gwt.charts.client.ChartWidget#draw(DataSource data, T
	 * options) ChartWidget}
	 * 
	 * @pre Both objects dataTable and options ar not null or empty
	 * 
	 * @post Drawn visualisation of the map
	 */
	public void draw() {
		super.draw(dataTable, options);
	}

}
