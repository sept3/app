package com.movieapp.client;

public enum MovieCriterias {
	WIKIID,
	FREEID,
	NAME,
	YEAR,
	REVENUE,
	RUNTIME,
	LANGUAGE,
	COUNTRY,
	GENRES
}
