package com.movieapp.client;

import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.BarChart;
import com.googlecode.gwt.charts.client.corechart.BarChartOptions;
import com.googlecode.gwt.charts.client.options.Annotations;
import com.googlecode.gwt.charts.client.options.AxisTitlesPosition;
import com.googlecode.gwt.charts.client.options.BackgroundColor;
import com.googlecode.gwt.charts.client.options.Bar;
import com.googlecode.gwt.charts.client.options.Orientation;;

@SuppressWarnings("unused")
public class MovieBarChart extends BarChart {

	private BarChartOptions options;
	private DataTable dataTable;

	/*
	 * Default Constructor that loads Options as JSObject
	 */
	public MovieBarChart(){
		options = BarChartOptions.create();
		//options.setOrientation(Orientation.HORIZONTAL);
	}

	/*
	 * Sets Annotations
	 * @pre options!=null
	 * @post BarChart object has annotation settings
	 */
	public void setAnnotations() {
		Annotations annotation = Annotations.create();
		//annotation.setBoxStyle(boxStyle);
		//annotation.setHighContrast(highContrast);
		//annotation.setTextStyle(textStyle);
		options.setAnnotations(annotation);
	}

	/*
	 * Sets the position of the axis titles. Here chosen by default is OUT 
	 * @pre options!=null
	 * @post BarCharts axis titles are out of the barchart
	 */
	public void setAxisTitlePosition() {
		options.setAxisTitlesPosition(AxisTitlesPosition.OUT);
	}

	/*
	 * Sets the Background color, the stroke and the stroke width of the chart
	 * @pre options!=null
	 * @post Background color, stroke and strokeWidth are set in the barchart diagram
	 * @param fill
	 * @param stroke
	 * @param strokeWidth
	 */
	public void setBackgroundColor(String fill, String stroke, int strokeWidth) {
		BackgroundColor color = BackgroundColor.create();
		color.setFill(fill);
		color.setStroke(stroke);
		color.setStrokeWidth(strokeWidth);
		options.setBackgroundColor(color);
	}

	
	/*
	 * Sets the width of each bar
	 * @pre options!=null
	 * @post each bar has a given width
	 * @param barWidth
	 */
	public void setBarWidth(int barWidth){
		Bar name = Bar.create();
		name.setGroupWidth(barWidth);
		options.setBar(name);
	}

	/*
	 * Sets a test-datatable
	 * @pre dataTable!=null
	 * @post BarChart has data to draw, which is shown in the drawn diagram
	 */
	public void setTestDataTable() {
		dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.NUMBER, "Y-Axis");
		dataTable.addColumn(ColumnType.NUMBER, "X-Axis");
		dataTable.addRows(3);
		dataTable.setValue(0, 0, 10);
		dataTable.setValue(0, 1, 23);
		dataTable.setValue(1, 0, 100);
		dataTable.setValue(1, 1, 15);
		dataTable.setValue(2, 0, 30);
		dataTable.setValue(2, 1, 24);
	}
	
	/*
	 * Draws the chart
	 * @pre options!=null && dataTable!=null
	 * @post BarChart object gets drawn within the container of the user interface
	 */
	public void draw(){
		super.draw(dataTable, options);
	}
}
