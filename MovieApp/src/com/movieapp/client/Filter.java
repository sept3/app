package com.movieapp.client;

import java.util.ArrayList;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;

public class Filter {
	
	private static MultiWordSuggestOracle oracleWID = new MultiWordSuggestOracle();
	private static MultiWordSuggestOracle oracleFID = new MultiWordSuggestOracle();
	private static MultiWordSuggestOracle oracleName = new MultiWordSuggestOracle();
	private static MultiWordSuggestOracle oracleYear = new MultiWordSuggestOracle();
	private static MultiWordSuggestOracle oracleRevenue = new MultiWordSuggestOracle();
	private static MultiWordSuggestOracle oracleRuntime = new MultiWordSuggestOracle();
	private static MultiWordSuggestOracle oracleLanguage = new MultiWordSuggestOracle();
	private static MultiWordSuggestOracle oracleCountry = new MultiWordSuggestOracle();
	private static MultiWordSuggestOracle oracleGenres = new MultiWordSuggestOracle();
	
	
	private static SuggestBox filterWID = new SuggestBox(oracleWID);
	private static SuggestBox filterFID = new SuggestBox(oracleFID);
	private static SuggestBox filterName = new SuggestBox(oracleName);
	private static SuggestBox filterYear = new SuggestBox(oracleYear);
	private static SuggestBox filterRevenue = new SuggestBox(oracleRevenue);
	private static SuggestBox filterRuntime = new SuggestBox(oracleRuntime);
	private static SuggestBox filterLanguage = new SuggestBox(oracleLanguage);
	private static SuggestBox filterCountry = new SuggestBox(oracleCountry);
	private static SuggestBox filterGenres = new SuggestBox(oracleGenres);
	
	/* Initialize the Filter and suggestboxes. */
	public static void initFilter()
	{
		filterWID.setSize("70px", "10px");	
		filterFID.setSize("70px", "10px");	
		filterName.setSize("70px", "10px");	
		filterYear.setSize("70px", "10px");	
		filterRevenue.setSize("70px", "10px");	
		filterRuntime.setSize("70px", "10px");	
		filterLanguage.setSize("70px", "10px");	
		filterCountry.setSize("70px", "10px");	
		filterGenres.setSize("70px", "10px");
		
		filterWID.getElement().setAttribute("placeholder", "Wiki ID");
		filterFID.getElement().setAttribute("placeholder", "Freebase ID");
		filterName.getElement().setAttribute("placeholder", "Name");
		filterYear.getElement().setAttribute("placeholder", "Year");
		filterRevenue.getElement().setAttribute("placeholder", "Revenue");
		filterRuntime.getElement().setAttribute("placeholder", "Runtime");
		filterLanguage.getElement().setAttribute("placeholder", "Language");
		filterCountry.getElement().setAttribute("placeholder", "Country");
		filterGenres.getElement().setAttribute("placeholder", "Genres");
	}
	
	/* Set the suggestboxes oracle. */
	public static void setSuggestBoxes()
	{
		
		DBServiceMethods.getCriteriaWID(0);
	 
		/*DBServiceMethods.getCriteriaFID(0);
	 
     	DBServiceMethods.getCriteriaName(0);
		
		DBServiceMethods.getCriteriaYear();
	 
		DBServiceMethods.getCriteriaRevenue();
	 
		DBServiceMethods.getCriteriaRuntime();
	 
		DBServiceMethods.getCriteriaLanguage();
	 
		DBServiceMethods.getCriteriaCountry();
	 
		DBServiceMethods.getCriteriaGenres();*/
	}
	
	/* Set the oracles. */
	public static void setOracles(MovieCriterias criteriaType, ArrayList<String> input)
	{
		switch (criteriaType)
		{
			case WIKIID:
			{
				for (int i=0; i<input.size(); i++)
				{
					oracleWID.add(input.get(i));
				}
				  
				filterWID.refreshSuggestionList();
			}
			break;
			case FREEID:
			{
				for (int i=0; i<input.size(); i++)
		    	  {
		    		  oracleFID.add(input.get(i));
		    	  }
		    	  
		    	  filterFID.refreshSuggestionList();
			}
			break;
			case NAME:
			{
				for (int i=0; i<input.size(); i++)
	      		{
	      			oracleName.add(input.get(i));
	      		}
		  
	      		filterName.refreshSuggestionList();
			}
			break;
			case YEAR:
			{
				for (int i=0; i<input.size(); i++)
				{
					oracleYear.add(input.get(i));
				}
				  
				filterYear.refreshSuggestionList();
			}
			break;
			case REVENUE:
			{
				for (int i=0; i<input.size(); i++)
				{
					oracleRevenue.add(input.get(i));
				}
				  
				filterRevenue.refreshSuggestionList();
			}
			break;
			case RUNTIME:
			{
				for (int i=0; i<input.size(); i++)
				{
					oracleRuntime.add(input.get(i));
				}
				  
				filterRuntime.refreshSuggestionList();
			}
			break;
			case LANGUAGE:
			{
				for (int i=0; i<input.size(); i++)
				{
					oracleLanguage.add(input.get(i));
				}
				  
				filterLanguage.refreshSuggestionList();
			}
			break;
			case COUNTRY:
			{
				for (int i=0; i<input.size(); i++)
				{
					oracleCountry.add(input.get(i));
				}
				  
				filterCountry.refreshSuggestionList();
			}
			break;
			case GENRES:
			{
				for (int i=0; i<input.size(); i++)
				{
					oracleGenres.add(input.get(i));
				}
				  
				filterGenres.refreshSuggestionList();
				Loading.getloadingScreen().hide();
			}
			break;
		}
	}
	

	
	public static SuggestBox getFilterWID() {
		return filterWID;
	}
	public static SuggestBox getFilterFID() {
		return filterFID;
	}
	public static SuggestBox getFilterName() {
		return filterName;
	}
	public static SuggestBox getFilterYear() {
		return filterYear;
	}
	public static SuggestBox getFilterRevenue() {
		return filterRevenue;
	}
	public static SuggestBox getFilterRuntime() {
		return filterRuntime;
	}
	public static SuggestBox getFilterLanguage() {
		return filterLanguage;
	}
	public static SuggestBox getFilterCountry() {
		return filterCountry;
	}
	public static SuggestBox getFilterGenres() {
		return filterGenres;
	}

}
