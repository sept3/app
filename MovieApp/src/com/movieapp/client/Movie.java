/*
 * Author: LP
 * Edited: LS
 * Defines the basics of a movie.
 */
package com.movieapp.client;

import java.io.Serializable;
import java.util.ArrayList;


public class Movie implements Serializable {
	/**
	 * 
	 */
	public static String sysOut = "";
	
	private static final long serialVersionUID = 1L;
	private String wikiMovID;
	private String freeBID;
	private String name;
	private String year;
	private int revenue;
	private int runtime;
	private ArrayList<String> language = new ArrayList <String>();
	private ArrayList<String> country = new ArrayList<String>();
	private ArrayList<String> genres = new ArrayList<String>();
	
	/* Initialize the Movie Object
	 * @param Wikipedia ID of Movie.
	 * @param Freebase ID of Movie.
	 * @param Name of Movie.
	 * @param Year of Movie.
	 * @param Revenue of Movie.
	 * @param Runtime of Movie.
	 * @param Language of Movie.
	 * @param Country of Movie.
	 * @param Genres of Movie.
	 */
	public void initMovie(String wikiMovID, String freeBID, String name, String year, String revenue, String runtime, String language, String country, String genres)
	//public void initMovie(String name, String year, String language, String country, String genres) 
	{
		try
		{
			if (year.length() > 4)
			{
				//String temp = year.substring(year.lastIndexOf("/"), year.length());
				this.year = year.substring(0, 4);//Integer.parseInt(temp);
			}
			else
			{
				this.year = year;//Integer.parseInt(year);
			}
		}
		catch (Exception e)
		{
			this.year = "0";
		}
		
		try
		{
			if (revenue.equals("") || runtime.equals(""))
			{
				if (!revenue.equals(""))
				{
					this.revenue = Integer.parseInt(revenue);
					this.runtime = 0;
				}
				else if (!runtime.equals(""))
				{
					this.revenue = 0;
					float temp;
					temp = Float.parseFloat(runtime);	
					this.runtime = Math.round(temp);
				}
				else
				{
					this.revenue = 0;
					this.runtime = 0;
				}
			}
			else
			{
				try
				{
					this.revenue = Integer.parseInt(revenue);
				}
				catch (Exception e)
				{
					this.revenue = 0;
				}
				float temp;
				temp = Float.parseFloat(runtime);	
				this.runtime = Math.round(temp);
			}
		}	
		catch (Exception e)
		{
			this.revenue = 0;
			this.runtime = 0;
		}
		int exceptionLocation = 0;
		
		try
		{
		int startWordGen = 0;
		int startWordLan = 0;
		int startWordCou = 0;
		boolean moreThanOne = false;
		for (int i=0; i<genres.length(); i++)
		{
			if (genres.charAt(i)==',')
			{
				exceptionLocation = 1;
				//tempStr = genres.substring(startWord, i);
				this.genres.add(genres.substring(startWordGen, i));
				startWordGen = i+1;
				moreThanOne = true;
			}	
		}
		if (!moreThanOne)
		{
			this.genres.add(genres);
		}
		else
		{
			moreThanOne = false;
		}
		for (int i=0; i<country.length(); i++)
		{
			if (country.charAt(i)==',')
			{
				exceptionLocation = 2;
				//tempStr = country.substring(startWord, i);
				this.country.add(country.substring(startWordCou, i));
				startWordCou = i+1;
				moreThanOne = true;
			}
		}
		if (!moreThanOne)
		{
			this.country.add(country);
		}
		else
		{
			moreThanOne = false;
		}
		for (int i=0; i<language.length(); i++)
		{
			if (language.charAt(i)==',')
			{
				exceptionLocation = 3;
				//tempStr = language.substring(startWord, i);
				
				this.language.add(language.substring(startWordLan, i));
				startWordLan = i+1;
				moreThanOne = true;
			}
		}
		if (!moreThanOne)
		{
			this.language.add(language);
		}
		else
		{
			moreThanOne = false;
		}
		
		}
		catch(Exception e)
		{
			if (exceptionLocation==1)
			{
				this.genres.clear();
				this.country.clear();
				this.language.clear();
			}
			else if (exceptionLocation ==2)
			{
				this.country.clear();
				this.language.clear();
			}
			else if (exceptionLocation == 3)
			{
				this.language.clear();
			}
		}
		this.wikiMovID = wikiMovID;
		this.freeBID = freeBID;
		this.name = name;
	}
	
	/* Represent Movie Object as String.*/
	@Override
	public String toString()
	{
		String allGen = "";
		String allCoun = "";
		String allLang = "";
		if (this.genres.size() != 0)
		{
			for (int i=0; i<this.genres.size(); i++)
			{
				if (i == 0)
				{
					allGen = this.genres.get(i);
				}
				else
				{
					allGen += ", " + this.genres.get(i);
				}
			}
		}
		else
		{}
		if (this.country.size() != 0)
		{
			for (int i=0; i<this.country.size(); i++)
			{
				if (i == 0)
				{
					allCoun = this.country.get(i);
				}
				else
				{
					allCoun += ", " + this.country.get(i);
				}
			}
		}
		else
		{}
		if (this.language.size() != 0)
		{
			for (int i=0; i<this.language.size(); i++)
			{
				if (i == 0)
				{
					allLang = this.language.get(i);
				}
				else
				{
					allLang += ", " + this.language.get(i);
				}
			}
		}
		else
		{}
		return "Wikipedia Movie ID: " + this.wikiMovID + " FreeBase ID: "
				+ this.freeBID + " Name: " + this.name + " Year: " + this.year 
				+ " Runtime: " + Integer.toString(this.runtime) + " Revenue: " + Integer.toString(this.revenue) 
				+ " Language: " + allLang + " Country: " 
				+ allCoun + " Genres: " + allGen;
	}
	
	public String getWikiMovID() {
		return wikiMovID;
	}

	public String getFreeBID() {
		return freeBID;
	}

	public int getRevenue() {
		return revenue;
	}

	public int getRuntime() {
		return runtime;
	}

	public String getName() {
		return name;
	}

	public String getYear() {
		return year;
	}
	
	public ArrayList<String> getLanguage() {
		return language;
	}

	public ArrayList<String> getCountry() {
		return country;
	}

	public ArrayList<String> getGenres() {
		return genres;
	}
}
