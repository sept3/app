/*
 * Author: LS
 * Modified:
 * Reads and administrates the movie lists and cleans the data.
 */
package com.movieapp.client;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Serializable;
import java.util.ArrayList;


public class MovieList implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String source;
	public ArrayList<Movie> movies = new ArrayList<Movie>();
	
	private boolean isLast = true;
	private int indexOfNext = 0;
	
	/* Initialize the MovieList 
	 * @param Name of the list.
	 * @param Source of the list.
	 */
	public void initMovieList(String name, String source)
	{
		this.name = name;
		this.source = source;
	}
	
	/* Add Movie to the MovieList
	 * @param Movie to add.
	 */
	public void addMovie(Movie movie)
	{
		movies.add(movie);
	}
	
	/* Get a Movie with the given criteria name
	 * @param Criterium name for the movie to find.
	 */
	public Movie getMovie(String criteria)
	{
		for (int i=0; i<movies.size(); i++)
		{
			if (movies.get(i).getName() == criteria)
			{
				return movies.get(i);
			}
		}
		return null;
	}
	
	
	public String getName()
	{
		return name;
	}
	
	public String getSource()
	{
		return source;
	}
	
	public ArrayList<Movie> getMovies()
	{
		return movies;
	}

	public boolean getIsLast() {
		return isLast;
	}

	public void setIsLast(boolean isLast) {
		this.isLast = isLast;
	}

	public int getIndexOfNext() {
		return indexOfNext;
	}

	public void setIndexOfNext(int indexOfNext) {
		this.indexOfNext = indexOfNext;
	}	
}
