package com.movieapp.client;

import java.util.ArrayList;
import java.util.LinkedList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("accessdb")
public interface DBService extends RemoteService {
	Boolean getCall(); //throws IllegalArgumentException;
	String getResult();
	LinkedList<Movie> testRPC();
	MovieList testObject();
	String readIntoDB();
	int[] loadCriteria(boolean isFirst, MovieCriterias criteriaType, int beginI, int beginJ);
	String sysOut();
	//ArrayList<String> getCriteria(MovieCriterias criteria);
	MovieList getMovieList(ArrayList<MovieCriterias> criteriaType, ArrayList<String> criteria);
	MovieList getNextList(int indexOfNext);
	ArrayList<String> getCriteriaWID(int nextIndex);
	ArrayList<String> getCriteriaFID(int nextIndex);
	ArrayList<String> getCriteriaName(int nextIndex);
	ArrayList<String> getCriteriaYear();
	ArrayList<String> getCriteriaRevenue();
	ArrayList<String> getCriteriaRuntime();
	ArrayList<String> getCriteriaLanguage();
	ArrayList<String> getCriteriaCountry();
	ArrayList<String> getCriteriaGenres();
}
