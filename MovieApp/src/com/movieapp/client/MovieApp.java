/*Edited: LS
 * 
 */
package com.movieapp.client;

import java.util.ArrayList;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DragEvent;
import com.google.gwt.event.dom.client.DragHandler;
import com.movieapp.client.LoginInfo;
import com.movieapp.client.LoginService;
import com.movieapp.client.LoginServiceAsync;


import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.kiouri.sliderbar.client.event.BarValueChangedEvent;
import com.kiouri.sliderbar.client.event.BarValueChangedHandler;
import com.kiouri.sliderbar.client.solution.adv.AdvancedSliderBar;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class MovieApp implements EntryPoint {

	private VerticalPanel mainPanel = new VerticalPanel();
	// private VerticalPanel tP = new VerticalPanel();
	private HorizontalPanel menubar = new HorizontalPanel();
	private TabPanel visualTabs = new TabPanel();
	private Button startDB = new Button("Load DB");

	// **********************DB Stuff***************************
	private HorizontalPanel filterMenu = new HorizontalPanel();
	private PopupPanel filterPop = new PopupPanel();
	private PopupPanel sysPan = new PopupPanel();
	private Button sysBut = new Button("System out");
	private VerticalPanel sysVP = new VerticalPanel();
	private Label sysLab = new Label();
	private Button filterBut = new Button("Filter");
	private Button filterSub = new Button("Submit");
	private VerticalPanel filterVer = new VerticalPanel();

	private String sysOut = "Start";

	// *******************DB Stuff*********************************

	// Admin Setup
	private LoginInfo loginInfo = null;
	private Anchor signInLink = new Anchor("Log in for Admin Mode");
	private Anchor signOutLink = new Anchor("Log out to leave Admin Mode");
	private VerticalPanel adminPanel = new VerticalPanel();
	private Label uploadInfoLabel = new Label("Upload Data");
	private FormPanel form = new FormPanel();
	private HorizontalPanel formPanel = new HorizontalPanel();

	// Slider Widget
	private AdvancedSliderBar adSlider = new AdvancedSliderBar();
	private Label valueLabel = new Label();
	private ListBox sliderBox = new ListBox();

	// Map Tab
	private VerticalPanel mapTabPanel = new VerticalPanel();
	private HorizontalPanel mapOptionsPanel = new HorizontalPanel();
	private RadioButton showAllButton = new RadioButton("group0", "Show all Data");
	private RadioButton showFilterButton = new RadioButton("group0", "Show only Filter Criteria");
	private RadioButton mapStyleButton1 = new RadioButton("group1", "Chart");
	private RadioButton mapStyleButton2 = new RadioButton("group1", "GoogleMap");
	private Button updateButton = new Button("Update");
	private ChartLoader chartLoader = new ChartLoader(ChartPackage.CORECHART);
	private ChartLoader mapLoader = new ChartLoader(ChartPackage.MAP);
	private HorizontalPanel googlePanel = new HorizontalPanel();
	private HorizontalPanel chartPanel = new HorizontalPanel();
	private HorizontalPanel sliderPanel = new HorizontalPanel();
	private WorldMarkerMap chartMap;
	private WorldMap googleMap;

	// Upload File
	private FlowPanel filePanel = new FlowPanel();
	private DBServiceAsync dsa = GWT.create(DBService.class);

	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	/*
	 * private static final String SERVER_ERROR = "An error occurred while " +
	 * "attempting to contact the server. Please check your network " +
	 * "connection and try again.";
	 * 
	 *//**
		 * Create a remote service proxy to talk to the server-side Greeting
		 * service.
		 *//*
		 * private final GreetingServiceAsync greetingService = GWT
		 * .create(GreetingService.class);
		 */

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		Loading.initLoading();
		Filter.initFilter();
		Startup.startupDB();
		Table.initFilterTable();

		visualTabs.add(adminPanel, "Admin");
		visualTabs.add(mapTabPanel, "Map");
		visualTabs.add(Table.getPanelTable(), "Table");

		// Setup Admin Tab
		LoginServiceAsync loginService = GWT.create(LoginService.class);
		loginService.login(GWT.getHostPageBaseURL(), new AsyncCallback<LoginInfo>() {
			public void onFailure(Throwable error) {
			}

			public void onSuccess(LoginInfo result) {
				loginInfo = result;
				if (loginInfo.isLoggedIn()) {
					loadAdminTab();
				} else {
					loadAdminLogin();
				}
			}
		});

		/*
		 * Setup for WorldMap
		 */
		Window.enableScrolling(true);
		Window.setMargin("0px");

		loadMapTab();
		// loadTable();
		// testSlider();

		visualTabs.selectTab(1);

		//menubar.add(startDB);
		// menubar.add(sysBut);
		//menubar.add(filterBut);

		// *******************DB Stuff*********************************
		startDB.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				DBServiceMethods.readyDB();
				// lbel.setText(DBServiceMethods.resultServer);
			}
		});
		sysPan.hide();
		sysPan.setAutoHideEnabled(true);
		sysPan.setSize("600px", "400px");
		sysPan.center();
		sysBut.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				sysPan.show();
				String output = sysOut;
				// DBServiceMethods.sysOut();
				// sysOut = DBServiceMethods.resultSysOut;
				/*
				 * for (int i=0;
				 * i<DBServiceMethods.getResultGetMovieList().get(0).getMovies()
				 * .size(); i++) { output += " " +
				 * DBServiceMethods.getResultGetMovieList().get(0).getMovies().
				 * get(i).toString(); }
				 */
				// output =
				// DBServiceMethods.getResultGetMovieList().get(0).getMovies().get(0).toString();
				output = RequestAPI.sysOut;
				sysLab.setText(output);
				sysPan.add(sysVP);
				sysVP.add(sysLab);
			}
		});

		filterPop.setSize("600px", "200px");
		filterPop.setAutoHideEnabled(true);
		filterPop.hide();
		filterPop.setPopupPosition(5, 200);

		filterBut.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				filterPop.show();
				// oracleYear.add("aaaaaa");
			}
		});
		filterSub.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ArrayList<String> input = new ArrayList<String>();
				input.clear();
				input.add(Filter.getFilterWID().getText());
				input.add(Filter.getFilterFID().getText());
				input.add(Filter.getFilterName().getText());
				input.add(Filter.getFilterYear().getText());
				input.add(Filter.getFilterRevenue().getText());
				input.add(Filter.getFilterRuntime().getText());
				input.add(Filter.getFilterLanguage().getText());
				input.add(Filter.getFilterCountry().getText());
				input.add(Filter.getFilterGenres().getText());
				sysOut = "Here";
				/*
				 * input.add(Filter.getFilter().getText());
				 * input.add(Filter.getFilterWID().getText());
				 * input.add(Filter.getFilterWID().getText());
				 */

				sysOut = "Here2";
				// String input = Filter.getFilterYear().getText();
				RequestAPI.getMovieList(input);
				sysOut = "Here3";
			}
		});

		filterVer.add(filterMenu);
		filterMenu.add(Filter.getFilterWID());
		filterMenu.add(Filter.getFilterFID());
		filterMenu.add(Filter.getFilterName());
		filterMenu.add(Filter.getFilterYear());
		filterMenu.add(Filter.getFilterRevenue());
		filterMenu.add(Filter.getFilterRuntime());
		filterMenu.add(Filter.getFilterLanguage());
		filterMenu.add(Filter.getFilterCountry());
		filterMenu.add(Filter.getFilterGenres());
		filterMenu.add(filterSub);
		// vP.add(tableResult.filterTable);

		// *******************DB Stuff*********************************

		RootPanel.get("menubar").add(menubar);
		RootPanel.get("menubar").add(filterVer);
		RootPanel.get("Visualisation").add(visualTabs);

	}

	private void markerMapSetup() {
		chartMap = new WorldMarkerMap();
		//chartMap.setDataTable();
		
		//DBServiceMethods.
		/*
		 * if (DBServiceMethods.resultGetMovieListML == null) {
		 *  } else {
		 * chartMap.setDataTable(DBServiceMethods.resultGetMovieListML); }
		 */
	}

	private void worldMapSetup() {
		googleMap = new WorldMap();
		/*
		 * if (DBServiceMethods.resultGetMovieListML == null) {
		 * googleMap.setDataTable(); } else {
		 * //googleMap.setDataTable(DBServiceMethods.resultGetMovieListML); }
		 */

	}
	
	private void loadSlider(){
		adSlider.setMaxValue(2015);
		adSlider.setNotSelectedInFocus();
		adSlider.setSize("500px", "20px");
		adSlider.addBarValueChangedHandler(new BarValueChangedHandler() {
			
			@Override
			public void onBarValueChanged(BarValueChangedEvent event) {
				valueLabel.setText(Integer.toString(adSlider.getValue()));				
				
			}
		});
		valueLabel.setSize("40px", "20px");
		valueLabel.setText(Integer.toString(adSlider.getValue()));
		
		sliderBox.addItem("Year");
		sliderBox.addItem("Runtime");
		sliderBox.addItem("Revenue");
		
		sliderPanel.add(adSlider);
		sliderPanel.add(valueLabel);
		sliderPanel.add(sliderBox);
		mapTabPanel.add(sliderPanel);

	}


	private void loadMapTab() {

		mapOptionsPanel.add(showAllButton);
		mapOptionsPanel.add(showFilterButton);
		mapOptionsPanel.add(mapStyleButton1);
		mapOptionsPanel.add(mapStyleButton2);
		mapOptionsPanel.add(updateButton);
		mapStyleButton1.setValue(true);
		showAllButton.setValue(true);
		mapTabPanel.add(mapOptionsPanel);

		chartPanel.setVisible(true);
		mapTabPanel.add(chartPanel);
		mapTabPanel.add(googlePanel);

		loadChart();
		loadSlider();
		updateButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if (mapStyleButton1.getValue()) {
					chartPanel.clear();
					googlePanel.clear();
					loadChart();
				} else {
					googlePanel.clear();
					chartPanel.clear();
					loadMap();
				}
			}

		});
	}

	void loadChart() {
		chartLoader.loadApi(new Runnable() {
			@Override
			public void run() {
				markerMapSetup();
				chartPanel.add(chartMap);
				chartMap.draw();
			}
		});
	}

	void loadMap() {
		mapLoader.loadApi(new Runnable() {
			@Override
			public void run() {
				worldMapSetup();
				googlePanel.add(googleMap);
				googleMap.draw();

			}

		});
	}

	/*
	 * private void loadTable() { Film testFilm1 = new Film("Test Movie",
	 * 123124, 3124, new ArrayList<String>(), new ArrayList<String>(), new
	 * ArrayList<String>()); table.list.add(testFilm1); table.createTable();
	 * visualTabs.add(table.getCellTable(), "Table"); }
	 */

	private void loadAdminLogin() {
		signInLink.setHref(loginInfo.getLoginUrl());
		adminPanel.add(signInLink);
	}

	private void loadAdminTab() {

		signOutLink.setHref(loginInfo.getLogoutUrl());
		adminPanel.add(signOutLink);
		form.setEncoding(FormPanel.ENCODING_MULTIPART);
		form.setMethod(FormPanel.METHOD_POST);

		FileUpload upload = new FileUpload();
		formPanel.add(uploadInfoLabel);
		formPanel.add(upload);
		form.add(formPanel);

		formPanel.add(new Button("Submit", new ClickHandler() {
			public void onClick(ClickEvent event) {
				form.submit();
			}
		}));

		form.addSubmitCompleteHandler(new SubmitCompleteHandler() {
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				Window.alert(event.getResults());
			}
		});

		adminPanel.add(form);
	}

}
