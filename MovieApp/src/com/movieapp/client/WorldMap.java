package com.movieapp.client;

import java.util.ArrayList;

import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.map.Map;
import com.googlecode.gwt.charts.client.map.MapOptions;
import com.googlecode.gwt.charts.client.options.MapType;

/*
 * Wrapper class for the Map-Class. Shows a world map as google maps window.
 *
 * @author Lenz
 * @version 1.0
 */

public class WorldMap extends Map {

	public static DataTable dataTable;
	public static MapOptions options;

	public WorldMap() {
		options = MapOptions.create();
		options.setEnableScrollWheel(true);
		options.setShowTip(true);
		options.setZoomLevel(2);
	}

	/*
	 * Sets the height of the Map
	 * 
	 * @pre options!=null
	 * 
	 * @post Map has fixed height
	 * 
	 * @param height
	 */
	public static void setHeight(int height) {
		options.setHeight(height);
	}

	/*
	 * Sets the width of the map
	 * 
	 * @pre options!=null
	 * 
	 * @post map has fixed width
	 * 
	 * @param width
	 */
	public static void setWidth(int width) {
		options.setWidth(width);
	}

	/*
	 * Sets both height and width of the map
	 * 
	 * @pre options!=null
	 * 
	 * @post map has both fixed height and width
	 * 
	 * @param height
	 * 
	 * @param width
	 */
	public static void setHeightWidth(int height, int width) {
		options.setHeight(height);
		options.setWidth(width);
	}

	/*
	 * Sets Map type: Possible Types are "Normal","Hybrid", "Satellite" or
	 * "terrain". It is possible to switch on a MapTypeControl
	 * 
	 * @pre options!=null
	 * 
	 * @post The map is shown as one of the four types, with our without user
	 * Control
	 * 
	 * @param maptype
	 * 
	 * @param showMapTypeControl
	 */
	public static void setMapType(String maptype, boolean showMapTypeControl) {
		options.setMapType(MapType.findByName(maptype));
		options.setUseMapTypeControl(showMapTypeControl);
	}

	/*
	 * Sets a few Markers as test cases for the map
	 * 
	 * @pre -
	 * 
	 * @post The field "dataTable" has some testdata
	 */
	public static void setDataTable() {
		dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.STRING, "Location");
		dataTable.addColumn(ColumnType.STRING, "Description");
	}

	public static void setDataTable(MovieList movieList) {
		dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.STRING, "Location");
		dataTable.addColumn(ColumnType.STRING, "Description");
		StringBuilder sb = new StringBuilder();
		ArrayList<Movie> movieArray = movieList.getMovies();
		dataTable.addRows(movieArray.size());
		for (int i = 0; i < movieArray.size(); i++) {
			sb.append(movieArray.get(i).getName());
			sb.append("\n");
			for (int j = 1; j < movieArray.size(); j++) {
				if (movieArray.get(i).getCountry().get(0).equalsIgnoreCase(movieArray.get(j).getCountry().get(0))) {
					if (!movieArray.get(i).getName().equalsIgnoreCase(movieArray.get(j).getName())) {
						sb.append(movieArray.get(j).getName());
						sb.append("\n");
					}
					movieArray.remove(j);
				}
			}
			dataTable.setValue(i, 0, movieArray.get(i).getCountry().get(0));
			dataTable.setValue(i, 1, sb.toString());
			sb.delete(0, sb.length());
		}

	}

	/*
	 * Draws the map with the input from the DataTabel object and the
	 * configuration of options
	 * 
	 * @pre dataTable!=null && options != null
	 * 
	 * @post The Map gets drawn
	 */
	public void draw() {
		super.draw(dataTable, options);
	}

	/*
	 * Converts an Array of type "Location" to a DataTable Object Just for test
	 * use
	 * 
	 * @pre -
	 * 
	 * @post -
	 * 
	 * @param locations
	 * 
	 * @return
	 */
	/*
	 * public DataTable Convert(Location[] locations) { dataTable =
	 * DataTable.create(); dataTable.addColumn(ColumnType.NUMBER, "Longitude");
	 * dataTable.addColumn(ColumnType.NUMBER, "Latitude");
	 * dataTable.addColumn(ColumnType.STRING, "Description");
	 * 
	 * for (int i = 0; i < locations.length; i++) { dataTable.addRow();
	 * dataTable.setValue(i, 0, locations[i].longitude); dataTable.setValue(i,
	 * 1, locations[i].latitude); dataTable.setValue(i, 2,
	 * locations[i].description); } return dataTable; }
	 */

}
