package com.movieapp.client;

import java.util.ArrayList;

import com.google.gwt.user.client.Timer;


public class RequestAPI {
	
	public static String sysOut = "Start";

	/* Man in the middle method to prepare the requests, to get movielist.
	 * 
	 */
	public static void getMovieList(ArrayList<String> input)
	{
		sysOut = "1";
		ArrayList<MovieCriterias> allCriteria = new ArrayList<MovieCriterias>();
		ArrayList<String> allInput = new ArrayList<String>();
		DBServiceMethods.clearResultGetMovieList();
		if (!(input.get(0).trim().equals("")))
		{
			allCriteria.add(MovieCriterias.WIKIID);
			allInput.add(input.get(0));
			DBServiceMethods.getMovieList(allCriteria, allInput);
		}
		else if (!(input.get(1).trim().equals("")))
		{
			allCriteria.add(MovieCriterias.FREEID);
			allInput.add(input.get(1));
			DBServiceMethods.getMovieList(allCriteria, allInput);
		}
		
		else if (!(input.get(2).trim().equals("")))
		{
			allCriteria.add(MovieCriterias.NAME);
			allInput.add(input.get(2));
			DBServiceMethods.getMovieList(allCriteria, allInput);
		}
		else
		{
			if (!(input.get(3).trim().equals("")))
			{
				allCriteria.add(MovieCriterias.YEAR);
				allInput.add(input.get(3));
			}
			if (!(input.get(4).trim().equals("")))
			{
				allCriteria.add(MovieCriterias.REVENUE);
				allInput.add(input.get(4));
			}
			if (!(input.get(5).trim().equals("")))
			{
				allCriteria.add(MovieCriterias.RUNTIME);
				allInput.add(input.get(5));
			}
			if (!(input.get(6).trim().equals("")))
			{
				allCriteria.add(MovieCriterias.LANGUAGE);
				allInput.add(input.get(6));
			}
			if (!(input.get(7).trim().equals("")))
			{
				allCriteria.add(MovieCriterias.COUNTRY);
				allInput.add(input.get(7));
			}
			if (!(input.get(8).trim().equals("")))
			{
				allCriteria.add(MovieCriterias.GENRES);
				allInput.add(input.get(8));
			}
			DBServiceMethods.getMovieList(allCriteria, allInput);
		}
		sysOut = "2";
		sysOut = input.get(0);
	}
	
	public static void getCallback(ArrayList<MovieList> input)
	{
		Table.refreshTable(combineMovieLists(input));
		WorldMarkerMap.setDataTable(combineMovieLists(input));
		WorldMap.setDataTable(combineMovieLists(input));
	}
	
	
	private static MovieList combineMovieLists(ArrayList<MovieList> input)
	{
		MovieList tempML = new MovieList();
		for (int i=0; i<input.size(); i++)
		{
			for (int j=0; j<input.get(i).getMovies().size();j++)
			{
				tempML.addMovie(input.get(i).getMovies().get(j));
			}
			//TODO: Source handling
		}
		
		return tempML;
	}
}
