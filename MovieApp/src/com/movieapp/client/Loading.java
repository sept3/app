package com.movieapp.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;

public class Loading {
	
	private static PopupPanel loadingScreen = new PopupPanel();
	private static Image loadingImage = new Image();
	
	/* Initiliaze the loading screen.*/
	public static void initLoading()
	{
		loadingScreen.setAutoHideEnabled(false);
		loadingScreen.setSize("1200px", "800px");
		loadingScreen.add(loadingImage);
		loadingImage.setUrl("adImage/loading.png");
	}
	
	public static PopupPanel getloadingScreen()
	{
		return loadingScreen;
	}

}
