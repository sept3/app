/*
 * Edited: LS
 */
 
package com.movieapp.client;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;



public class Table
{
	private static FlexTable filterTable = new FlexTable();
	private static Label resultRange = new Label();
	private static Label totalRange = new Label();
	private static VerticalPanel panelTable = new VerticalPanel();
	private static Hyperlink previousList = new Hyperlink();
	private static Hyperlink nextList = new Hyperlink();
	private static HorizontalPanel controlList = new HorizontalPanel();
	private static String sysOut = "Start";
	private static MovieList tempML = new MovieList();
	private static int indexOfFirst = 0;
	
	/* Initialize the table. */
	@SuppressWarnings("deprecation")
	public static void initFilterTable()
	{
		filterTable.setStyleName("Flextable");
		filterTable.setText(0, 0, "Wikipedia ID");
		filterTable.setText(0, 1, "FreeBase ID");
		filterTable.setText(0, 2, "Name");
		filterTable.setText(0, 3, "Year");
		filterTable.setText(0, 4, "Revenue");
		filterTable.setText(0, 5, "Runtime");
		filterTable.setText(0, 6, "Language");
		filterTable.setText(0, 7, "Country");
		filterTable.setText(0, 8, "Genres");
		previousList.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event)
			{
				if(tempML.getMovies().size()>1000)
				{
					filterTable.removeAllRows();
					indexOfFirst -=1000;
					filterTable.setText(0, 0, "Wikipedia ID");
					filterTable.setText(0, 1, "FreeBase ID");
					filterTable.setText(0, 2, "Name");
					filterTable.setText(0, 3, "Year");
					filterTable.setText(0, 4, "Revenue");
					filterTable.setText(0, 5, "Runtime");
					filterTable.setText(0, 6, "Language");
					filterTable.setText(0, 7, "Country");
					filterTable.setText(0, 8, "Genres");
					for (int i=indexOfFirst;i<tempML.getMovies().size()&&i<indexOfFirst+1000;i++)
					{
						int row = filterTable.getRowCount();
						filterTable.setText(row, 0, tempML.getMovies().get(i).getWikiMovID());
						filterTable.setText(row, 1, tempML.getMovies().get(i).getFreeBID());
						filterTable.setText(row, 2, tempML.getMovies().get(i).getName());
						filterTable.setText(row, 3, tempML.getMovies().get(i).getYear());
						filterTable.setText(row, 4, String.valueOf(tempML.getMovies().get(i).getRevenue()));
						filterTable.setText(row, 5, String.valueOf(tempML.getMovies().get(i).getRuntime()));
						filterTable.setText(row, 6, tempML.getMovies().get(i).getLanguage().get(0));
						filterTable.setText(row, 7, tempML.getMovies().get(i).getCountry().get(0));
						filterTable.setText(row, 8, tempML.getMovies().get(i).getGenres().get(0));
					}
					nextList.setVisible(true);
					if (indexOfFirst == 0)
					{
						previousList.setVisible(false);
					}
					resultRange.setText("Results: " + Integer.toString(indexOfFirst) + " - " + Integer.toString(indexOfFirst+1000));
				}
			}
		});
		
		nextList.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event)
			{
				if(tempML.getMovies().size()>1000)
				{
					filterTable.removeAllRows();
					indexOfFirst +=1000;
					filterTable.setText(0, 0, "Wikipedia ID");
					filterTable.setText(0, 1, "FreeBase ID");
					filterTable.setText(0, 2, "Name");
					filterTable.setText(0, 3, "Year");
					filterTable.setText(0, 4, "Revenue");
					filterTable.setText(0, 5, "Runtime");
					filterTable.setText(0, 6, "Language");
					filterTable.setText(0, 7, "Country");
					filterTable.setText(0, 8, "Genres");
					int endRange = 0;
					for (int i=indexOfFirst;i<tempML.getMovies().size()&&i<indexOfFirst+1000;i++)
					{
						int row = filterTable.getRowCount();
						filterTable.setText(row, 0, tempML.getMovies().get(i).getWikiMovID());
						filterTable.setText(row, 1, tempML.getMovies().get(i).getFreeBID());
						filterTable.setText(row, 2, tempML.getMovies().get(i).getName());
						filterTable.setText(row, 3, tempML.getMovies().get(i).getYear());
						filterTable.setText(row, 4, String.valueOf(tempML.getMovies().get(i).getRevenue()));
						filterTable.setText(row, 5, String.valueOf(tempML.getMovies().get(i).getRuntime()));
						filterTable.setText(row, 6, tempML.getMovies().get(i).getLanguage().get(0));
						filterTable.setText(row, 7, tempML.getMovies().get(i).getCountry().get(0));
						filterTable.setText(row, 8, tempML.getMovies().get(i).getGenres().get(0));
					}
					if (tempML.getMovies().size()<(1000+indexOfFirst))
					{
						nextList.setVisible(false);
					}
					previousList.setVisible(true);
					if (indexOfFirst+1000>tempML.getMovies().size())
					{
						endRange = tempML.getMovies().size();
					}
					else
					{
						endRange = indexOfFirst+1000;
					}
					resultRange.setText("Results: " + Integer.toString(indexOfFirst) + " - " + Integer.toString(endRange));
				}
			}
		});
		previousList.setText("Previous");
		nextList.setText("Next");
		previousList.setSize("80px", "10px");
		nextList.setSize("80px", "10px");
		resultRange.setSize("600px", "10px");
		previousList.setVisible(false);
		nextList.setVisible(false);
		resultRange.setVisible(false);
		totalRange.setVisible(false);
		controlList.add(previousList);
		controlList.add(nextList);
		controlList.add(resultRange);
		controlList.add(totalRange);
		panelTable.add(controlList);
		panelTable.add(filterTable);
		applyDataRowStyles();
		
	}

	private static void applyDataRowStyles() {
		HTMLTable.RowFormatter rf = filterTable.getRowFormatter();

		for (int row = 0; row < filterTable.getRowCount(); ++row) {
			if ((row % 2) != 0) {
				rf.addStyleName(row, "FlexTable-OddRow");
			}
			else {
				rf.addStyleName(row, "FlexTable-EvenRow");
			}
		}
		rf.addStyleName(0, "FlexTable-TitleRow");
	}
	
	/* Refresh the table with a new MovieList
	 * @param A new movielist that should be shown.
	 */
	public static void refreshTable(MovieList input) 
	{
		int endOfRange = 0;
		if (input.getMovies().size()> 1000)
		{
			endOfRange = 1000;
		}
		else
		{
			endOfRange = input.getMovies().size();
		}
		tempML.getMovies().clear();
		sysOut = "1";
		resultRange.setText("Results: 0 - " + Integer.toString(endOfRange));
		resultRange.setVisible(true);
		totalRange.setText("Number of Results: " + Integer.toString(input.getMovies().size()));
		totalRange.setVisible(true);
		nextList.setVisible(false);
		previousList.setVisible(false);
		filterTable.removeAllRows();
		indexOfFirst +=1000;
		filterTable.setText(0, 0, "Wikipedia ID");
		filterTable.setText(0, 1, "FreeBase ID");
		filterTable.setText(0, 2, "Name");
		filterTable.setText(0, 3, "Year");
		filterTable.setText(0, 4, "Revenue");
		filterTable.setText(0, 5, "Runtime");
		filterTable.setText(0, 6, "Language");
		filterTable.setText(0, 7, "Country");
		filterTable.setText(0, 8, "Genres");
				
		for(int i=0; i<input.getMovies().size()&&i<1000; i++)
		{
			int row = filterTable.getRowCount();
			filterTable.setText(row, 0, input.getMovies().get(i).getWikiMovID());
			filterTable.setText(row, 1, input.getMovies().get(i).getFreeBID());
			filterTable.setText(row, 2, input.getMovies().get(i).getName());
			filterTable.setText(row, 3, input.getMovies().get(i).getYear());
			filterTable.setText(row, 4, String.valueOf(input.getMovies().get(i).getRevenue()));
			filterTable.setText(row, 5, String.valueOf(input.getMovies().get(i).getRuntime()));
			filterTable.setText(row, 6, input.getMovies().get(i).getLanguage().get(0));
			filterTable.setText(row, 7, input.getMovies().get(i).getCountry().get(0));
			filterTable.setText(row, 8, input.getMovies().get(i).getGenres().get(0));
			//panelTable.add(filterTable);
			
		}
		if (input.getMovies().size()>1000)
		{
			nextList.setVisible(true);
			indexOfFirst = 0;
			for (int j=0;j<input.getMovies().size();j++)
			{
				tempML.addMovie(input.getMovies().get(j));
			}
		}
			
		sysOut = "3";
		
		applyDataRowStyles();
	}
	
	

	public static VerticalPanel getPanelTable() {
		return panelTable;
	}
	/*public FlexTable getPanelTable() {
		return filterTable;
	}*/
}