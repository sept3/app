package com.movieapp.client;

import java.util.ArrayList;
import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class DBServiceMethods {
	
	public static String failureMes = "Start";
	public static String sysOut = "Start";
	
	private static DBServiceAsync dsa = GWT.create(DBService.class);
	
	public static void reqServerResult() {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				String d = "2";
			}

			public void onSuccess(String result) {
				resultCallback(result);
			}
		};
		dsa.getResult(callback);
	}
	
	public static String movieReqResult = "";
	private static void resultCallback(String input) {
		//returnCallback = input;
		movieReqResult = input;
	}
	
	/*Request server to prepare the DB*/
	public static void readyDB() {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				String d = "2";
			}

			public void onSuccess(String result) {
				setResultString(result);
			}
		};
		dsa.readIntoDB(callback);
	}
	
	public static String resultServer = "";
	private static void setResultString(String input) {
		resultServer = input;
		loadCriteria(true, MovieCriterias.WIKIID, 0, 0);
	}

	
	/* Check if db has to get loaded or not. */
	public static void getCall() {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
			public void onFailure(Throwable caught) {
				String d = "2";
			}

			public void onSuccess(Boolean result) {
				setReturnCallback(result);
			}
		};
		dsa.getCall(callback);
	}

	private static void setReturnCallback(boolean call) {
		if (call)
		{
			Startup.startOK();
		}
		else
		{
			Loading.getloadingScreen().show();
			Filter.setSuggestBoxes();
		}
	}

	public static void reqTRPC() {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<MovieList> callback = new AsyncCallback<MovieList>() {
			public void onFailure(Throwable caught) {
				failureMes = caught.toString();
			}

			public void onSuccess(MovieList result) {
				setTRPC(result);
			}
		};
		dsa.testObject(callback);
	}

	public static String testRPC = "";
	private static void setTRPC(MovieList input) {
		//returnCallback = input;
		testRPC = input.getMovies().get(0).toString();
	}
	

	/* Request the server to load all possible criteria.
	 * @param Handlig for bigger request: Is the first request.
	 * @param Handlig for bigger request: In which criteria the server is right now.
	 * @param Handlig for bigger request: Beginning of index I.
	 * @param Handling for bigger request: Beginning of index J.
	 */
	public static void loadCriteria(boolean isFirst, MovieCriterias criteriaType, int beginI, int beginJ) {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<int[]> callback = new AsyncCallback<int[]>() {
			public void onFailure(Throwable caught) {
				failureMes = caught.toString();
			}

			public void onSuccess(int[] result) {
				setLoadCriteria(result);
			}
		};
		dsa.loadCriteria(isFirst, criteriaType, beginI, beginJ, callback);
	}
	
	private static void setLoadCriteria(int[] input) {
		MovieCriterias criteria = MovieCriterias.WIKIID;
		if (input[0]==1)
		{
			switch (input[1])
			{
				case 0:
				{
					criteria = MovieCriterias.WIKIID;
				}
				break;
				case 1:
				{
					criteria = MovieCriterias.FREEID;
				}
				break;
				case 2:
				{
					criteria = MovieCriterias.NAME;
				}
				break;
				case 3:
				{
					criteria = MovieCriterias.YEAR;
				}
				break;
				case 4:
				{
					criteria = MovieCriterias.REVENUE;
				}
				break;
				case 5:
				{
					criteria = MovieCriterias.RUNTIME;
				}
				break;
				case 6:
				{
					criteria = MovieCriterias.LANGUAGE;
				}
				break;
				case 7:
				{
					criteria = MovieCriterias.COUNTRY;
				}
				break;
				case 8:
				{
					criteria = MovieCriterias.GENRES;
				}
				break;
			}
			loadCriteria(false, criteria, input[2], input[3]);
		}
		else
		{
			Filter.setSuggestBoxes();
		}
	}
	
	public static void sysOut() {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				String d = "2";
			}

			public void onSuccess(String result) {
				setSysOut(result);
			}
		};
		dsa.sysOut(callback);
	}

	public static String resultSysOut = "";
	private static void setSysOut(String input) {
		resultSysOut = input;
	}
	

	
	public static ArrayList<String> resultGetCriteriaWID = new ArrayList<String>();
	public static ArrayList<String> resultGetCriteriaFID = new ArrayList<String>();
	public static ArrayList<String> resultGetCriteriaName = new ArrayList<String>();
	public static ArrayList<String> resultGetCriteriaYear = new ArrayList<String>();
	public static ArrayList<String> resultGetCriteriaRevenue = new ArrayList<String>();
	public static ArrayList<String> resultGetCriteriaRuntime = new ArrayList<String>();
	public static ArrayList<String> resultGetCriteriaLanguage = new ArrayList<String>();
	public static ArrayList<String> resultGetCriteriaCountry = new ArrayList<String>();
	public static ArrayList<String> resultGetCriteriaGenres = new ArrayList<String>();
	
	
	/* Get possible criterias for the suggestboxes.
	 * @param For the server, if list is longer, its the index of the next list from server. 
	 */
	public static void getCriteriaWID(int nextInt) {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
			public void onFailure(Throwable caught) {
				failureMes = caught.toString();
			}

			public void onSuccess(ArrayList<String> result) {
				setGetCriteriaWID(result);
			}
		};
		dsa.getCriteriaWID(nextInt, callback);
	}
	
	private static void setGetCriteriaWID(ArrayList<String> input) {
		int nextIndex = 0;
		
		if (input.size() == 10001)
		{
			nextIndex = Integer.parseInt(input.get(10000));
			input.remove(10000);
			
		}
		for (int i=0;i<input.size();i++)
		{
			resultGetCriteriaWID.add(input.get(i));
		}
		
		if (nextIndex != 0)
		{
			getCriteriaWID(nextIndex);
			
		}
		else
		{
			Filter.setOracles(MovieCriterias.WIKIID, resultGetCriteriaWID);
			DBServiceMethods.getCriteriaFID(0);
		}
		
	}		
	
	
	
	public static void getCriteriaFID(int nextIndex) {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
			public void onFailure(Throwable caught) {
				failureMes = caught.toString();
			}

			public void onSuccess(ArrayList<String> result) {
				setGetCriteriaFID(result);
			}
		};
		dsa.getCriteriaFID(nextIndex, callback);
	}
	
	private static void setGetCriteriaFID(ArrayList<String> input) {
		int nextIndex = 0;
		
		if (input.size() == 10001)
		{
			nextIndex = Integer.parseInt(input.get(10000));
			input.remove(10000);
			
		}
		for (int i=0;i<input.size();i++)
		{
			resultGetCriteriaFID.add(input.get(i));
		}
		
		if (nextIndex != 0)
		{
			getCriteriaFID(nextIndex);
			
		}
		else
		{
			Filter.setOracles(MovieCriterias.FREEID, resultGetCriteriaFID);
			DBServiceMethods.getCriteriaName(0);
		}
	}	
	
	public static void getCriteriaName(int nextIndex) {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
			public void onFailure(Throwable caught) {
				failureMes = caught.toString();
			}

			public void onSuccess(ArrayList<String> result) {
				setGetCriteriaName(result);
			}
		};
		dsa.getCriteriaName(nextIndex, callback);
	}
	
	private static void setGetCriteriaName(ArrayList<String> input) {
		int nextIndex = 0;
		
		if (input.size() == 10001)
		{
			nextIndex = Integer.parseInt(input.get(10000));
			input.remove(10000);
			
		}
		for (int i=0;i<input.size();i++)
		{
			resultGetCriteriaName.add(input.get(i));
			
		}
		
		if (nextIndex != 0)
		{
			getCriteriaName(nextIndex);
			
		}
		else
		{
			Filter.setOracles(MovieCriterias.NAME, resultGetCriteriaName);
			DBServiceMethods.getCriteriaYear();
		}
	}	
	
	
	
	public static void getCriteriaYear() {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
			public void onFailure(Throwable caught) {
				failureMes = caught.toString();
			}

			public void onSuccess(ArrayList<String> result) {
				setGetCriteriaYear(result);
			}
		};
		dsa.getCriteriaYear(callback);
	}
	
	private static void setGetCriteriaYear(ArrayList<String> input) {
		
		resultGetCriteriaYear = input;
		Filter.setOracles(MovieCriterias.YEAR, input);
		DBServiceMethods.getCriteriaRevenue();
	}
	
	
	public static void getCriteriaRevenue() {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
			public void onFailure(Throwable caught) {
				failureMes = caught.toString();
			}

			public void onSuccess(ArrayList<String> result) {
				setGetCriteriaRevenue(result);
			}
		};
		dsa.getCriteriaRevenue(callback);
	}
	
	private static void setGetCriteriaRevenue(ArrayList<String> input) {
		
		resultGetCriteriaRevenue = input;
		Filter.setOracles(MovieCriterias.REVENUE, input);
		DBServiceMethods.getCriteriaRuntime();
	}
	
	
	public static void getCriteriaRuntime() {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
			public void onFailure(Throwable caught) {
				failureMes = caught.toString();
			}

			public void onSuccess(ArrayList<String> result) {
				setGetCriteriaRuntime(result);
			}
		};
		dsa.getCriteriaRuntime(callback);
	}
	
	
	private static void setGetCriteriaRuntime(ArrayList<String> input) {
		
		resultGetCriteriaRuntime = input;
		Filter.setOracles(MovieCriterias.RUNTIME, input);
		DBServiceMethods.getCriteriaLanguage();
	}
	
	
	public static void getCriteriaLanguage() {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
			public void onFailure(Throwable caught) {
				failureMes = caught.toString();
			}

			public void onSuccess(ArrayList<String> result) {
				setGetCriteriaLanguage(result);
			}
		};
		dsa.getCriteriaLanguage(callback);
	}
	
	
	private static void setGetCriteriaLanguage(ArrayList<String> input) {
		
		resultGetCriteriaLanguage = input;
		Filter.setOracles(MovieCriterias.LANGUAGE, input);
		DBServiceMethods.getCriteriaCountry();
	}
	
	public static void getCriteriaCountry() {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
			public void onFailure(Throwable caught) {
				failureMes = caught.toString();
			}

			public void onSuccess(ArrayList<String> result) {
				setGetCriteriaCountry(result);
			}
		};
		dsa.getCriteriaCountry(callback);
	}
	
	
	private static void setGetCriteriaCountry(ArrayList<String> input) {
		
		resultGetCriteriaCountry = input;
		Filter.setOracles(MovieCriterias.COUNTRY, input);
		DBServiceMethods.getCriteriaGenres();
	}
	
	public static void getCriteriaGenres() {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
			public void onFailure(Throwable caught) {
				failureMes = caught.toString();
			}

			public void onSuccess(ArrayList<String> result) {
				setGetCriteriaGenres(result);
			}
		};
		dsa.getCriteriaGenres(callback);
	}
	
	
	private static void setGetCriteriaGenres(ArrayList<String> input) {
		
		resultGetCriteriaGenres = input;
		Filter.setOracles(MovieCriterias.GENRES, input);
	}
	
	
	/*
	 * So this is an important method. Right now the way to access the DB. 
	 * It gives back a Movie (just one) and a String: resultGetMovieList, resultGetMovieListM both static in this class.
	 * @params: Enum of criteriatype although there are 6 only 4 are accessible. Integerhandling problem. There are more
	 * filter criterias but these are getting prepared by database to have a choice, but they are not in the search
	 * implemented yet.
	 * @params: The criteria of the criteriatype you want e.g: criteriaType: YEAR (enum), criteria: "1990"
	 *  
	 */
	public static void getMovieList(ArrayList<MovieCriterias> criteriaType, ArrayList<String> criteria) {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		sysOut = "1";
		AsyncCallback<MovieList> callback = new AsyncCallback<MovieList>() {
			public void onFailure(Throwable caught) {
				String d = "2";
			}

			public void onSuccess(MovieList result) {
				setGetMovieList(result);
			}
		};
		dsa.getMovieList(criteriaType, criteria, callback);
	}

	private static String resultGetMovieList = "";
	private static Movie resultGetMovieListM = null;
	private static ArrayList<MovieList> resultGetMovieListML = new ArrayList<MovieList>();
	private static void setGetMovieList(MovieList input) {
		//sysOut = "2";
		//resultGetMovieListM = input.getMovies().get(0);
		sysOut = "Size " + Integer.toString(input.getMovies().size());
		resultGetMovieListML.add(input);
		/*for (int i=0; i< 1; i++)
		{
			resultGetMovieList += "   " + input.getMovies().get(i).toString();
		}*/
		
		if (!(input.getIsLast()))
		{
			getNextList(DBServiceMethods.resultGetMovieListML
				.get(DBServiceMethods.resultGetMovieListML.size()-1).getIndexOfNext());
		}
		else
		{
			sysOut = "3";
			RequestAPI.getCallback(resultGetMovieListML);
			
		}
		
	}
	
	/* Request the next list, created for the handling of bigger rpc requests.
	 * @param Index of the next list that should get requested.
	 */
	public static void getNextList(int indexOfNext) {
		if (dsa == null) {
			dsa = GWT.create(DBService.class);
		}
		AsyncCallback<MovieList> callback = new AsyncCallback<MovieList>() {
			public void onFailure(Throwable caught) {
				String d = "2";
			}

			public void onSuccess(MovieList result) {
				setGetMovieList(result);
			}
		};
		dsa.getNextList(indexOfNext, callback);
	}
	
	public static ArrayList<MovieList> getResultGetMovieList()
	{
		return resultGetMovieListML;
	}
	
	public static void clearResultGetMovieList()
	{
		resultGetMovieListML.clear();
	}
}
