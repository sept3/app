package com.movieapp.client;

import java.util.ArrayList;
import java.util.LinkedList;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface DBServiceAsync {
	void getCall(AsyncCallback<Boolean> callback);
			//throws IllegalArgumentException;
	void getResult(AsyncCallback<String> callback);
	void testRPC(AsyncCallback<LinkedList<Movie>> callback);
	void testObject(AsyncCallback<MovieList> callback);
	void readIntoDB(AsyncCallback<String> callback);
	void loadCriteria(boolean isFirst, MovieCriterias criteriaType, int beginI, int beginJ, AsyncCallback<int[]> callback);
	void sysOut(AsyncCallback<String> callback);
	//void getCriteria(MovieCriterias criteria, AsyncCallback<ArrayList<String>> callback);
	void getMovieList(ArrayList<MovieCriterias> criteriaType, ArrayList<String> criteria, AsyncCallback<MovieList> callback);
	void getNextList(int indexOfNext, AsyncCallback<MovieList> callback);
	void getCriteriaWID(int nextIndex, AsyncCallback<ArrayList<String>> callback);
	void getCriteriaFID(int nextIndex, AsyncCallback<ArrayList<String>> callback);
	void getCriteriaName(int nextIndex, AsyncCallback<ArrayList<String>> callback);
	void getCriteriaYear(AsyncCallback<ArrayList<String>> callback);
	void getCriteriaRevenue(AsyncCallback<ArrayList<String>> callback);
	void getCriteriaRuntime(AsyncCallback<ArrayList<String>> callback);
	void getCriteriaLanguage(AsyncCallback<ArrayList<String>> callback);
	void getCriteriaCountry(AsyncCallback<ArrayList<String>> callback);
	void getCriteriaGenres(AsyncCallback<ArrayList<String>> callback);

}
